# PaintBBS(お絵かき掲示板) #

* お絵かき掲示板SHIGEMIのソースコードになります。(http://paint-shigemi.net)

#開発環境#
* Eclipse(Luna)
* Java1.8
* Servlet3.1
* JSP2.3
* PostgreSQL9.3
* JavaScript
* jQuery2.1

# ソースについて #

* Mavenプロジェクトになりますが、機密情報保護のために一部ソースを変更しています。

* ソースの確認のみに利用してください。



