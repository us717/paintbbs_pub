package jp.co.us717.general;

import static org.junit.Assert.*;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import jp.co.us717.testfunc.FuncDBSetup;
import jp.co.us717.testfunc.FuncJNDISetup;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import org.junit.AfterClass;
/**
 * データベース接続テスト。
 */
public class DBConnectTest {

    /** JNDI */
    private FuncJNDISetup jndi_;

    /**
     * JNDIセットアップ、データバックアップ。
     */
    @BeforeClass
    public static void BeforeClass() {
        FuncJNDISetup.SetJNDI();
        FuncJNDISetup tmpJNDI = new FuncJNDISetup();
        tmpJNDI.loadSetting();
        FuncDBSetup.Backup(tmpJNDI.getDataSource());
        FuncDBSetup.InsertTestData(tmpJNDI.getDataSource(), "src/test/resources/sqlTestData/DBConnectData.xml");
    }

    /**
     * テスト前処理。
     */
    @Before
    public void Before(){
        jndi_ = new FuncJNDISetup();
        jndi_.loadSetting();
    }

    /**
     * データベース復元。
     */
    @AfterClass
    public static void AfterClass() {
        FuncJNDISetup tmpJNDI = new FuncJNDISetup();
        tmpJNDI.loadSetting();
        FuncDBSetup.Restore(tmpJNDI.getDataSource());
    }

    /**
     * 挿入チェック。
     * @throws Exception エラー全般
     */
    @Test
    public void test_挿入チェック() throws Exception {
        try (Connection con = jndi_.getDataSource().getConnection();) {
            //挿入データ取得
            PreparedStatement statement = con.prepareStatement("select thread_id from thread");
            ResultSet result = statement.executeQuery();
            result.next();
            assertEquals("3", result.getString("thread_id"));
        }
    }
}
