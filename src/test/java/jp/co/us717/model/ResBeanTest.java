package jp.co.us717.model;

import static org.junit.Assert.*;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Field;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

import jp.co.us717.exception.SQLRuntimeException;
import jp.co.us717.testfunc.FuncDBSetup;
import jp.co.us717.testfunc.FuncJNDISetup;
import static org.hamcrest.CoreMatchers.*;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * ResBeanのテスト。
 */
public class ResBeanTest {

    /** JNDI */
    private FuncJNDISetup jndi_;

    /**
     * JNDIセットアップ、データバックアップ。
     */
    @BeforeClass
    public static void BeforeClass() {
        FuncJNDISetup.SetJNDI();
        FuncJNDISetup tmpJNDI = new FuncJNDISetup();
        tmpJNDI.loadSetting();
        FuncDBSetup.Backup(tmpJNDI.getDataSource());
    }

    /**
     * テスト前処理。
     */
    @Before
    public void Before() {
        jndi_ = new FuncJNDISetup();
        jndi_.loadSetting();
        FuncDBSetup.Delete(jndi_.getDataSource());
        FuncDBSetup.InsertTestData(jndi_.getDataSource(), "src/test/resources/sqlTestData/ResBeanTest.xml");
    }

    /**
     * データベース復元。
     */
    @AfterClass
    public static void AfterClass() {
        FuncJNDISetup tmpJNDI = new FuncJNDISetup();
        tmpJNDI.loadSetting();
        FuncDBSetup.Restore(tmpJNDI.getDataSource());
    }

    /**
     * 正常なデータを渡すと投稿は成功する。<br>
     * thread_id(3)のレスが一件登録される。<br>
     * 画像データも保存されている(目視確認)
     * @throws Exception エラー全般
     */
    @Test
    public void InsertRes_正常なレス投稿() throws Exception {
        ResBean res = new ResBean();

        //テスト用画像データ準備
        BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(
                "src/test/resources/testPaintDate.txt")));
        StringBuilder sb = new StringBuilder();
        String line;
        while ((line = reader.readLine()) != null) {
            sb.append(line);
        }
        reader.close();

        //テスト
        res.setDBConnect(jndi_.getDataSource());
        res.setImageData(sb.toString());
        res.setThreadId(3);
        res.setAddres("2");
        res.setSaveLocation("src/test/resources/testimage");
        res.InsertRes();

        //チェック
        try (Connection con = jndi_.getDataSource().getConnection();
                Statement stm = con.createStatement();) {
            ResultSet result = stm.executeQuery("select *from res where thread_id=3");

            int resCount = 0;
            while (result.next()) {
                assertThat(result.getString("thread_id"), is("3"));
                assertThat(result.getString("poster"), is("名無し"));
                assertThat(result.getString("state"), is("t"));
                resCount++;
            }
            assertThat(resCount, is(1));
        }
    }

    /**
     * スレが書き込み禁止だった場合、例外(SQLRuntimeException)が発生する。
     * @throws Exception エラー全般
     */
    @Test(expected = SQLRuntimeException.class)
    public void InsertRes_スレが投稿禁止() throws Exception {
        ResBean res = new ResBean();

        //テスト用画像データ準備
        BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(
                "src/test/resources/testPaintDate.txt")));
        StringBuilder sb = new StringBuilder();
        String line;
        while ((line = reader.readLine()) != null) {
            sb.append(line);
        }
        reader.close();

        //テスト
        res.setDBConnect(jndi_.getDataSource());
        res.setImageData(sb.toString());
        res.setThreadId(2);
        res.setSaveLocation("src/test/resources/testimage");
        res.InsertRes();
    }

    /**
     * 違反データでデータベースに挿入できなかった場合、例外(SQLRuntimeException)が発生する。<br>
     * スレッド番号を設定していない。
     * @throws Exception エラー全般
    */
    @Test(expected = SQLRuntimeException.class)
    public void InsertRes_異常なDB用データ() throws Exception {
        ResBean res = new ResBean();

        //テスト用画像データの準備
        BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(
                "src/test/resources/testPaintDate.txt")));
        StringBuilder sb = new StringBuilder();
        String line;
        while ((line = reader.readLine()) != null) {
            sb.append(line);
        }
        reader.close();

        //テスト(スレ番号を設定していない)
        res.setDBConnect(jndi_.getDataSource());
        res.setSaveLocation("src/test/resources/testimage");
        res.setImageData(sb.toString());
        res.InsertRes();
    }

    /**
     * 書き込み禁止の成功。
     * @exception Exception エラー全般
     */
    @Test
    public void ProhibitionOfResShowing_書き込み禁止の成功() throws Exception {
        ResBean res = new ResBean();
        res.setDBConnect(jndi_.getDataSource());

        //テスト
        res.setThreadId(1);
        res.setResNum(1);
        res.setPassword("1010");
        res.ProhibitionOfResShowing();

        //チェック
        try (Connection con = jndi_.getDataSource().getConnection();
                Statement stm = con.createStatement()) {
            String sql = "select filename from res where state = false";
            ResultSet rs = stm.executeQuery(sql);
            while (rs.next()) {
                assertThat(rs.getString(1), is("1.jpg"));
            }
        }
    }

    /**
     * 書き込み禁止の失敗でSQLRuntimeExceptionが返る。
     * @throws Exception エラー全般
     */
    @Test(expected = SQLRuntimeException.class)
    public void ProhibitionOfResShowing_書き込み禁止の失敗() throws Exception {
        ResBean res = new ResBean();
        res.setDBConnect(jndi_.getDataSource());

        //テスト
        res.setThreadId(5);
        res.setResNum(1);
        res.setPassword("1010");
        res.ProhibitionOfResShowing();

        //チェック
        try (Connection con = jndi_.getDataSource().getConnection();
                Statement stm = con.createStatement()) {
            String sql = "select filename from res where state =false";
            ResultSet rs = stm.executeQuery(sql);
            while (rs.next()) {
                assertThat(rs.getString(1), is("1.jpg"));
            }
        }
    }

    /*
     * NULLを渡すと例外(NullPointerException)が発生する。
     */
    @Test(expected = NullPointerException.class)
    public void setDBConnect_Nullを挿入() {
        ResBean res = new ResBean();
        res.setDBConnect(null);
    }

    /**
     * 正常値を渡すと結果はフィールドに反映される。
     * @throws Exception エラー全般
     */
    @Test
    public void setPoster_正常な挿入() throws Exception {
        //テスト設定
        ResBean res = new ResBean();
        res.setPoster("漢字漢字");

        //privateなフィールドにアクセスできるようにする。
        Field field = res.getClass().getDeclaredField("poster_");
        field.setAccessible(true);
        String result = "漢字漢字";

        //チェック
        assertThat(field.get(res), is(result));
    }

    /**
     * NULLの場合、結果は反映されない。
     * @throws Exception エラー全般
     */
    @Test
    public void setPoster_Nullの挿入() throws Exception {
        //テスト設定
        ResBean res = new ResBean();
        res.setPoster(null);

        //privateなフィールドにアクセスできるようにする。
        Field field = res.getClass().getDeclaredField("poster_");
        field.setAccessible(true);
        String result = "名無し";

        //チェック
        assertThat(field.get(res), is(result));
    }

    /**
     * 空文字の場合、結果は反映されない。
     * @throws Exception エラー全般
     */
    @Test
    public void setPoster_空文字の挿入() throws Exception {
        //テスト設定
        ResBean res = new ResBean();
        String data = new String("");
        res.setPoster(data);

        //privateなフィールドにアクセスできるようにする。
        Field field = res.getClass().getDeclaredField("poster_");
        field.setAccessible(true);
        String result = "名無し";

        //チェック
        assertThat(field.get(res), is(result));
    }

    /**
     * スペースの場合、結果は反映されない。
     * @throws Exception エラー全般
     */
    @Test
    public void setPoster_スペースの挿入() throws Exception {
        //テスト設定
        ResBean res = new ResBean();
        String data = new String("   ");
        res.setPoster(data);

        //privateなフィールドにアクセスできるようにする。
        Field field = res.getClass().getDeclaredField("poster_");
        field.setAccessible(true);
        String result = "名無し";

        //チェック
        assertThat(field.get(res), is(result));
    }

    /**
     * 半角英数を挿入するとフィールドに反映される。
     * @throws Exception エラー全般
     */
    @Test
    public void setPassword_正常な挿入() throws Exception {
        //テスト設定
        ResBean res = new ResBean();
        res.setPassword("Test1234");

        //privateなフィールドにアクセスできるようにする。
        Field password = res.getClass().getDeclaredField("password_");
        Field isPassword = res.getClass().getDeclaredField("isPassword_");
        password.setAccessible(true);
        isPassword.setAccessible(true);

        //チェック
        assertThat(password.get(res), is("Test1234"));
        assertThat(isPassword.get(res), is(true));
    }

    /**
     * 全角が含まれていると反映されない。
     * @throws Exception エラー全般
     */
    @Test
    public void setPassword_全角が含まれている挿入() throws Exception {
        //テスト設定
        ResBean res = new ResBean();
        res.setPassword("test１２３４");

        //privateなフィールドにアクセスできるようにする。
        Field password = res.getClass().getDeclaredField("password_");
        Field isPassword = res.getClass().getDeclaredField("isPassword_");
        password.setAccessible(true);
        isPassword.setAccessible(true);

        //チェック
        assertThat(password.get(res), is("0000"));
        assertThat(isPassword.get(res), is(false));
    }

    /**
     * NULLの場合反映されない。
     * @throws Exception エラー全般
     */
    @Test
    public void setPassword_NULLの場合() throws Exception {
        //テスト設定
        ResBean res = new ResBean();
        res.setPassword(null);

        //privateなフィールドにアクセスできるようにする。
        Field password = res.getClass().getDeclaredField("password_");
        Field isPassword = res.getClass().getDeclaredField("isPassword_");
        password.setAccessible(true);
        isPassword.setAccessible(true);

        //チェック
        assertThat(password.get(res), is("0000"));
        assertThat(isPassword.get(res), is(false));
    }

    /**
     * 空文字の場合反映されない。
     * @throws Exception エラー全般
     */
    @Test
    public void setPassword_空文字の場合() throws Exception {
        //テスト設定
        ResBean res = new ResBean();
        res.setPassword("");

        //privateなフィールドにアクセスできるようにする。
        Field password = res.getClass().getDeclaredField("password_");
        Field isPassword = res.getClass().getDeclaredField("isPassword_");
        password.setAccessible(true);
        isPassword.setAccessible(true);

        //チェック
        assertThat(password.get(res), is("0000"));
        assertThat(isPassword.get(res), is(false));
    }

    /**
     * スペースの場合反映されない。
     * @throws Exception エラー全般
     */
    @Test
    public void setPassword_スペースの場合() throws Exception {

        //テスト設定
        ResBean res = new ResBean();
        res.setPassword(" ");

        //privateなフィールドにアクセスできるようにする。
        Field password = res.getClass().getDeclaredField("password_");
        Field isPassword = res.getClass().getDeclaredField("isPassword_");
        password.setAccessible(true);
        isPassword.setAccessible(true);

        //チェック
        assertThat(password.get(res), is("0000"));
        assertThat(isPassword.get(res), is(false));
    }

}
