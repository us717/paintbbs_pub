package jp.co.us717.model;

import jp.co.us717.testfunc.FuncDBSetup;
import jp.co.us717.testfunc.FuncJNDISetup;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

/**
 * ShowThreadBeanのテスト。
 */
public class ThreadDataBeanTest {

    /** JNDI */
    private FuncJNDISetup jndi_;

    /**
     * JNDIセットアップ、データバックアップ。
     */
    @BeforeClass
    public static void BeforeClass() {
        FuncJNDISetup.SetJNDI();
        FuncJNDISetup tmpJNDI = new FuncJNDISetup();
        tmpJNDI.loadSetting();
        FuncDBSetup.Backup(tmpJNDI.getDataSource());
    }

    /**
     * テスト前処理。
     */
    @Before
    public void Before() {
        jndi_ = new FuncJNDISetup();
        jndi_.loadSetting();
        FuncDBSetup.Delete(jndi_.getDataSource());
        FuncDBSetup.InsertTestData(jndi_.getDataSource(), "src/test/resources/sqlTestData/ResListBeanTest.xml");
    }

    /**
     * データベース復元。
     */
    @AfterClass
    public static void AfterClass() {
        FuncJNDISetup tmpJNDI = new FuncJNDISetup();
        tmpJNDI.loadSetting();
        FuncDBSetup.Restore(tmpJNDI.getDataSource());
    }

    /**
     * スレッドの情報が作成されているか。<br>
     * thread_id(2)を呼び出す。
     */
    @Test
    public void SelectThreadData_スレッドの情報が作成されているか() {

        //テスト
        ThreadDataBean threadData = new ThreadDataBean();
        threadData.setDBConnect(jndi_.getDataSource());
        threadData.setThreadId(2);
        threadData.SelectThreadData();

        //チェック
        assertThat(threadData.getThreadInfo().getThreadId(), is(2));
    }

    /**
     * 存在しないIDの場合は空のスレッド情報となる。
     */
    @Test
    public void SelectThreadData_存在しないIDの場合は空のスレッド情報となる() {

        //テスト
        ThreadDataBean threadData = new ThreadDataBean();
        threadData.setDBConnect(jndi_.getDataSource());
        threadData.setThreadId(100);
        threadData.SelectThreadData();

        //チェック
        assertThat(threadData.getThreadInfo().getThreadId(), is(0));
    }

    /**
     * レスリストが作成されているか。<br>
     * thread_id(2)を呼び出す。
     */
    @Test
    public void SelectThreadData_レスリストが作成されているか() {

        //テスト
        ThreadDataBean threadData = new ThreadDataBean();
        threadData.setDBConnect(jndi_.getDataSource());
        threadData.setThreadId(2);
        threadData.SelectThreadData();

        //チェック
        assertThat(threadData.getResList().size(), is(4));
        assertThat(threadData.getResList().get(0).getImagePath(), is("4.jpg"));
    }

    /**
     * 存在しないIDの場合は空のレスリストとなる。
     */
    @Test
    public void SelecThreadData_存在しないIDの場合は空のレスリストとなる() {

        //テスト
        ThreadDataBean threadData = new ThreadDataBean();
        threadData.setDBConnect(jndi_.getDataSource());
        threadData.setThreadId(100);
        threadData.SelectThreadData();

        //チェック
        assertThat(threadData.getResList().size(), is(0));
    }
}
