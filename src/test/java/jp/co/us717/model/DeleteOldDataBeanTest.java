package jp.co.us717.model;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

import static org.junit.Assert.*;
import jp.co.us717.testfunc.FuncDBSetup;
import jp.co.us717.testfunc.FuncJNDISetup;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.*;

import org.junit.AfterClass;

/**
 * DeleteOldDataBeanのテスト。
 */
public class DeleteOldDataBeanTest {

    /** JNDI */
    private FuncJNDISetup jndi_;

    /**
     * JNDIセットアップ、データバックアップ。
     */
    @BeforeClass
    public static void BeforeClass() {
        FuncJNDISetup.SetJNDI();
        FuncJNDISetup tmpJNDI = new FuncJNDISetup();
        tmpJNDI.loadSetting();
        FuncDBSetup.Backup(tmpJNDI.getDataSource());
    }

    /**
     * テスト前処理。
     */
    @Before
    public void Before() {
        jndi_ = new FuncJNDISetup();
        jndi_.loadSetting();
        FuncDBSetup.Delete(jndi_.getDataSource());
        FuncDBSetup.InsertTestData(jndi_.getDataSource(), "src/test/resources/sqlTestData/DeleteOldDataBeanTest.xml");
    }

    /**
     * データベース復元。
     */
    @AfterClass
    public static void AfterClass() {
        FuncJNDISetup tmpJNDI = new FuncJNDISetup();
        tmpJNDI.loadSetting();
        FuncDBSetup.Restore(tmpJNDI.getDataSource());
    }

    /**
      * 正常処理でスレッドが書き込み禁止になる。<br>
      * 4以上を書き込み禁止に指定すると、thread_id(5,6,7)が書き込み禁止に指定される。
     * @throws Exception エラー全般
      */
    @Test
    public void InhibitThread_正しい処理() throws Exception {
        DeleteOldDataBean deleteData = new DeleteOldDataBean();
        deleteData.setDBConnect(jndi_.getDataSource());
        deleteData.ChangeThreadNoPosting(4);

        try (Connection con = jndi_.getDataSource().getConnection();
                Statement stm = con.createStatement()) {

            ResultSet result = stm.executeQuery("select *from thread where state = false order by thread_id");
            int i = 0;
            String r[] = { "5", "6", "7" };
            while (result.next()) {
                i++;
                assertThat(result.getString("thread_id"), is(r[i - 1]));
            }

            assertThat(i, is(3));
        }
    }

    /**
     * 正常処理で古いデータは削除される。<br>
     * 4以上を指定すると、thread_id(1,2,3,4)のみデータは存在することになる。
     * @throws Exception エラー全般
     */
    @Test
    public void DeleteOldData() throws Exception {
        DeleteOldDataBean deleteData = new DeleteOldDataBean();
        deleteData.setDBConnect(jndi_.getDataSource());
        deleteData.setSaveLocation("D:/EAS/workspace/PaintBBS/src/test/resources/testimage");
        deleteData.DeleteOldData(4);

        try (Connection con = jndi_.getDataSource().getConnection();
                Statement stm = con.createStatement()) {

            ResultSet result = stm.executeQuery("select *from thread order by thread_id");
            int i = 0;
            String r[] = { "1", "2", "3", "4" };
            while (result.next()) {
                i++;
                assertThat(result.getString("thread_id"), is(r[i - 1]));
            }

            assertThat(i, is(4));
        }
    }
}
