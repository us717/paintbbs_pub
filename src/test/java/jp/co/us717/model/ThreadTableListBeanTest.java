package jp.co.us717.model;

import static org.junit.Assert.*;
import jp.co.us717.testfunc.FuncDBSetup;
import jp.co.us717.testfunc.FuncJNDISetup;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.hamcrest.CoreMatchers.*;
/**
 * ThreadTableListBeanのテスト
 */
public class ThreadTableListBeanTest {

    /** JNDI */
    private FuncJNDISetup jndi_;

    /**
     * JNDIセットアップ、データバックアップ。
     */
    @BeforeClass
    public static void BeforeClass() {
        FuncJNDISetup.SetJNDI();
        FuncJNDISetup tmpJNDI = new FuncJNDISetup();
        tmpJNDI.loadSetting();
        FuncDBSetup.Backup(tmpJNDI.getDataSource());
    }

    /**
     * テスト前処理。
     */
    @Before
    public void Before() {
        jndi_ = new FuncJNDISetup();
        jndi_.loadSetting();
        FuncDBSetup.Delete(jndi_.getDataSource());
        FuncDBSetup.InsertTestData(jndi_.getDataSource(), "src/test/resources/sqlTestData/ThreadTableListBeanTest.xml");
    }

    /**
     * データベース復元。
     */
    @AfterClass
    public static void AfterClass() {
        FuncJNDISetup tmpJNDI = new FuncJNDISetup();
        tmpJNDI.loadSetting();
        FuncDBSetup.Restore(tmpJNDI.getDataSource());
    }

    @Test
    /**
     * 正常にスレッド一覧リストが作成される。
     */
    public void SelectThreadList_正常に作成される() {
        ThreadTableListBean threadTable = new ThreadTableListBean();

        //テスト
        threadTable.setDBConnect(jndi_.getDataSource());
        threadTable.SelectThreadList(30);

        //チェック
        assertThat(threadTable.getThreadTableList().size(), is(3));
        assertThat(threadTable.getThreadTableList().get(0).getThreadId(), is(1));
        assertThat(threadTable.getThreadTableList().get(1).getThreadId(), is(2));
        assertThat(threadTable.getThreadTableList().get(2).getThreadId(), is(3));
    }

    @Test
    /**
     * 範囲外は空のリストが返される。
     */
    public void SelectThreadList_範囲外() {
        ThreadTableListBean threadTable = new ThreadTableListBean();

        //テスト
        threadTable.setDBConnect(jndi_.getDataSource());
        threadTable.SelectThreadList(0);

        //チェック
        assertThat(threadTable.getThreadTableList().size(), is(0));
    }
}
