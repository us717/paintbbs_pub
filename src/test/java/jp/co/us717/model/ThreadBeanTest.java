package jp.co.us717.model;

import static org.junit.Assert.*;


import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

import jp.co.us717.exception.SQLRuntimeException;
import jp.co.us717.testfunc.FuncDBSetup;
import jp.co.us717.testfunc.FuncJNDISetup;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.*;
import org.junit.AfterClass;

/**
 * ThreadBeanのテスト。
 */
public class ThreadBeanTest {

    /** JNDI */
    private FuncJNDISetup jndi_;

    /**
     * JNDIセットアップ、データバックアップ。
     */
    @BeforeClass
    public static void BeforeClass() {
        FuncJNDISetup.SetJNDI();
        FuncJNDISetup tmpJNDI = new FuncJNDISetup();
        tmpJNDI.loadSetting();
        FuncDBSetup.Backup(tmpJNDI.getDataSource());
    }

    /**
     * テスト前処理。
     */
    @Before
    public void Before(){
        jndi_ = new FuncJNDISetup();
        jndi_.loadSetting();
        FuncDBSetup.Delete(jndi_.getDataSource());
    }

    /**
     * データベース復元。
     */
    @AfterClass
    public static void AfterClass() {
        FuncJNDISetup tmpJNDI = new FuncJNDISetup();
        tmpJNDI.loadSetting();
        FuncDBSetup.Restore(tmpJNDI.getDataSource());
    }

    /**
     * 正しい処理でスレッドが作成される。<br>
     * @throws Exception エラー全般
     **/
    @Test
    public void CreateTheread_正しい処理() throws Exception {
        //テスト
        ThreadBean createThread = new ThreadBean();
        createThread.setDBConnect(jndi_.getDataSource());
        createThread.setWidth(250);
        createThread.setHeight(200);
        createThread.setThreadName("スレッド作成テスト");
        createThread.CreateThread();

        //チェック
        try (Connection con = jndi_.getDataSource().getConnection();
                Statement stm = con.createStatement();) {
            ResultSet result = stm.executeQuery("select *from thread");

            int i = 0;
            while (result.next()) {
                i++;
                assertThat(i, is(1));//１つしか作成されていない。
                assertThat(result.getString("thread_name"), is("スレッド作成テスト"));
                assertThat(result.getString("width"), is("250"));
                assertThat(result.getString("height"), is("200"));
                assertThat(result.getString("state"), is("t"));
                assertThat(result.getString("res_count"), is("0"));
                assertNotNull(result.getString("res_count"));
            }
        }
    }

    /**
     * データに不備があると例外が発生する。
     */
    @Test(expected = SQLRuntimeException.class)
    public void CreateTheread_データに不備がある() {
        //テスト
        ThreadBean Thread = new ThreadBean();
        Thread.setDBConnect(jndi_.getDataSource());
        Thread.setWidth(250);
        Thread.setThreadName("スレッド作成テスト");
        Thread.CreateThread();
    }

}
