package jp.co.us717.model;

import jp.co.us717.testfunc.FuncDBSetup;
import jp.co.us717.testfunc.FuncJNDISetup;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

import jp.co.us717.lib.ContentsTypeEnum;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

/**
 * DeleteRequestBeanのテスト。<br>
 */
public class DeletionRequestBeanTest {

    /** JNDI */
    private FuncJNDISetup jndi_;

    /**
     * JNDIセットアップ、データバックアップ。
     */
    @BeforeClass
    public static void BeforeClass() {
        FuncJNDISetup.SetJNDI();
        FuncJNDISetup tmpJNDI = new FuncJNDISetup();
        tmpJNDI.loadSetting();
        FuncDBSetup.Backup(tmpJNDI.getDataSource());
    }

    /**
     * テスト前処理。
     */
    @Before
    public void Before() {
        jndi_ = new FuncJNDISetup();
        jndi_.loadSetting();
        FuncDBSetup.Delete(jndi_.getDataSource());
    }

    /**
     * データベース復元。
     */
    @AfterClass
    public static void AfterClass() {
        FuncJNDISetup tmpJNDI = new FuncJNDISetup();
        tmpJNDI.loadSetting();
        FuncDBSetup.Restore(tmpJNDI.getDataSource());
    }

    /**
     * 正常にデータベースに登録ができるか。
     * @throws Exception エラー全般
     */
    @Test
    public void SelectHeadline_正常にデータベースに登録できるか() throws Exception {

        //テスト
        DeletionRequestBean delReq = new DeletionRequestBean();
        delReq.setDBConnect(jndi_.getDataSource());
        delReq.setRequestType(ContentsTypeEnum.RES);
        delReq.setThreadId(1);
        delReq.setThreadNum(1);
        delReq.setIpAddres("192.168.118.0/24");
        delReq.setReason("テスト");
        delReq.RegisterRequestResDelete();

        //チェック
        try(Connection con = jndi_.getDataSource().getConnection();
                Statement stm = con.createStatement()) {
            String sql = "select *from deletion_request";
            ResultSet rs = stm.executeQuery(sql);
            while(rs.next()) {
                assertThat(rs.getInt("thread_id"), is(1));
            }
        }
    }

}