package jp.co.us717.model;

import jp.co.us717.testfunc.FuncDBSetup;
import jp.co.us717.testfunc.FuncJNDISetup;

import java.sql.Connection;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

/**
 * HeadlineBeanのテスト。<br>
 */
public class HeadlineBeanTest {

    /** JNDI */
    private FuncJNDISetup jndi_;

    /**
     * JNDIセットアップ、データバックアップ。
     */
    @BeforeClass
    public static void BeforeClass() {
        FuncJNDISetup.SetJNDI();
        FuncJNDISetup tmpJNDI = new FuncJNDISetup();
        tmpJNDI.loadSetting();
        FuncDBSetup.Backup(tmpJNDI.getDataSource());
    }

    /**
     * テスト前処理。
     */
    @Before
    public void Before() {
        jndi_ = new FuncJNDISetup();
        jndi_.loadSetting();
        FuncDBSetup.Delete(jndi_.getDataSource());
        FuncDBSetup.InsertTestData(jndi_.getDataSource(), "src/test/resources/sqlTestData/HeadlineBeanTest.xml");
    }

    /**
     * データベース復元。
     */
    @AfterClass
    public static void AfterClass() {
        FuncJNDISetup tmpJNDI = new FuncJNDISetup();
        tmpJNDI.loadSetting();
        FuncDBSetup.Restore(tmpJNDI.getDataSource());
    }

    /**
     * 正常に新着レスが作成できているか。
     */
    @Test
    public void SelectHeadline_新着レスが正しく作成されているか() {

        //スレッド一覧作成
        ThreadTableListBean threadTable = new ThreadTableListBean();
        threadTable.setDBConnect(jndi_.getDataSource());
        threadTable.SelectThreadList(30);

        //テスト
        HeadlineBean headline = new HeadlineBean();
        headline.setDBConnect(jndi_.getDataSource());
        headline.setNumberOfNewRes(3);
        headline.setNumberOfArticle(5);
        headline.setPageNumber(0);
        headline.setThreadTableList(threadTable.getThreadTableList());
        headline.SelectHeadline();

        //チェック
        HashMap<Integer, ArrayList<ResBean>> data = headline.getHeadlineResTable();
        assertThat(data.get(2).size(), is(3));
        assertThat(data.get(8).size(), is(3));
        assertThat(data.get(9).size(), is(2));

        assertThat(data.get(8).get(0).getImagePath(), is("8.jpg"));
        assertThat(data.get(8).get(1).getImagePath(), is("6.jpg"));
        assertThat(data.get(8).get(2).getImagePath(), is("7.jpg"));
    }

    @Test
    /**
     * 正常にスレッドリストが作成されているか。
     */
    public void SelectHeadline_スレッドリストが正しく作成されているか() {
        //スレッド一覧作成
        ThreadTableListBean threadTable = new ThreadTableListBean();
        threadTable.setDBConnect(jndi_.getDataSource());
        threadTable.SelectThreadList(30);

        //テスト
        HeadlineBean headline = new HeadlineBean();
        headline.setDBConnect(jndi_.getDataSource());
        headline.setNumberOfNewRes(2);
        headline.setNumberOfArticle(3);
        headline.setPageNumber(1);
        headline.setThreadTableList(threadTable.getThreadTableList());
        headline.SelectHeadline();

        //チェック
        int resCount = 0;
        int thread_id[] = { 8, 4, 7, };
        int res_count[] = { 2, 0, 0, };
        assertThat(headline.getHeadlineThreadList().size(), is(3));
        for (ThreadBean i : headline.getHeadlineThreadList()) {
            assertThat(i.getThreadId(), is(thread_id[resCount]));
            assertThat(i.getResCount(), is(res_count[resCount]));
            resCount++;
        }
    }

    /**
     * threadTableListをnull指定で例外が発生する。
     */
    @Test(expected = NullPointerException.class)
    public void SelectHeadline_threadTableListがnull() {
        //テスト
        HeadlineBean headline = new HeadlineBean();
        headline.setNumberOfNewRes(2);
        headline.setNumberOfArticle(3);
        headline.setPageNumber(1);
        headline.setThreadTableList(null);
        headline.SelectHeadline();
        headline.setDBConnect(jndi_.getDataSource());
    }

    /**
     * threadTableListが空の場合、スレッドリストは空のリストになる。
     */
    @Test
    public void SelectHeadline_thraedTableListが空() {
        //テスト
        HeadlineBean headline = new HeadlineBean();
        headline.setDBConnect(jndi_.getDataSource());
        headline.setNumberOfNewRes(2);
        headline.setNumberOfArticle(3);
        headline.setPageNumber(1);
        headline.setThreadTableList(new ArrayList<ThreadBean>());
        headline.SelectHeadline();

        //チェック
        assertThat(headline.getHeadlineThreadList().size(), is(0));
    }

    /**
     * 引数に指定したthradTableListのレス数は通常の場合、変更されない。
     */
    @Test
    public void SelectHeadline_thraedTableListは通常は変更されない() {
        //スレッド一覧作成
        ThreadTableListBean threadTable = new ThreadTableListBean();
        threadTable.setDBConnect(jndi_.getDataSource());
        threadTable.SelectThreadList(30);

        //テスト
        HeadlineBean headline = new HeadlineBean();
        headline.setDBConnect(jndi_.getDataSource());
        headline.setNumberOfNewRes(2);
        headline.setNumberOfArticle(3);
        headline.setPageNumber(1);
        headline.setThreadTableList(threadTable.getThreadTableList());
        headline.SelectHeadline();

        //チェック(thread_id=8)
        assertThat(threadTable.getThreadTableList().get(3).getResCount(), is(3));
    }

    /**
     * スレッド一覧とヘッドライン作成の間にレスが挿入されると、threadTableListは最新のレス数が代入される。
     * @throws Exception エラー全般
     */
    @Test
    public void SelectHeadline_処理中のレス投稿でthraedTableListのレス数が変更される() throws Exception {
        //スレッド一覧作成
        ThreadTableListBean threadTable = new ThreadTableListBean();
        threadTable.setDBConnect(jndi_.getDataSource());
        threadTable.SelectThreadList(30);

        //レスを投稿
        try (Connection con = jndi_.getDataSource().getConnection();
                Statement stm = con.createStatement()) {
            stm.execute("insert into res(thread_id, poster, filename, postdate, is_password, password, state, addres)values"
                    + " (7, '名無し', '11.jpg', now(), false, '0000', true, 1)");
        }
        //テスト
        HeadlineBean headline = new HeadlineBean();
        headline.setDBConnect(jndi_.getDataSource());
        headline.setNumberOfNewRes(2);
        headline.setNumberOfArticle(3);
        headline.setPageNumber(1);
        headline.setThreadTableList(threadTable.getThreadTableList());
        headline.SelectHeadline();

        //チェック(thread_id=7)
        assertThat(threadTable.getThreadTableList().get(5).getResCount(), is(1));
        ArrayList<ResBean> data = headline.getHeadlineResTable().get(7);
        assertThat(data.size(), is(1));
    }
}