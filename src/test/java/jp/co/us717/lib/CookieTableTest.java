package jp.co.us717.lib;

import javax.servlet.http.Cookie;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import org.junit.Test;

/**
 * CookiTableのテスト。
 */
public class CookieTableTest {

    /**
     * クッキーテーブルが作成される。
     */
    @Test
    public void CreateCookieTable_クッキーテーブルが作成される() {
        Cookie[] testData = { new Cookie("test1", "v1"), new Cookie("test2", "v2"), new Cookie("test3", "v3") };

        //テスト
        CookieTable table = new CookieTable();
        table.CreateCookieTable(testData);

        //チェック
        assertThat(table.getTableSize(), is(3));
    }

    /**
     * nullを渡しても空のテーブルのままである。
     */
    @Test
    public void CreateCookieTable_nullを渡す() {

        //テスト
        CookieTable table = new CookieTable();
        table.CreateCookieTable(null);

        //チェック
        assertThat(table.getTableSize(), is(0));
    }

    /**
     * 指定したクッキーが返る。
     */
    @Test
    public void getCookie_指定したクッキーが返る() {
        Cookie[] testData = { new Cookie("test1", "v1"), new Cookie("test2", "v2"), new Cookie("test3", "v3") };

        //テスト
        CookieTable table = new CookieTable();
        table.CreateCookieTable(testData);

        //チェック
        assertThat(table.getCookie("test1").getValue(), is("v1"));
    }

    /**
     * 指定したクッキーが存在しない時はnullが返る。
     */
    @Test
    public void getCookie_存在しない場合はnullが返る() {
        Cookie[] testData = { new Cookie("test1", "v1"), new Cookie("test2", "v2"), new Cookie("test3", "v3") };

        //テスト
        CookieTable table = new CookieTable();
        table.CreateCookieTable(testData);

        //チェック
        assertThat(table.getCookie("test4"), is(nullValue()));
    }

}
