package jp.co.us717.lib;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;

import jp.co.us717.exception.SQLRuntimeException;
import jp.co.us717.testfunc.FuncDBSetup;
import jp.co.us717.testfunc.FuncJNDISetup;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.AfterClass;

/**
 * DatabeseFunctionのテスト。
 *
 */
public class DatabaseFunctionTest {

    /** JNDI */
    private FuncJNDISetup jndi_;

    /**
     * JNDIセットアップ、データバックアップ。
     */
    @BeforeClass
    public static void BeforeClass() {
        FuncJNDISetup.SetJNDI();
        FuncJNDISetup tmpJNDI = new FuncJNDISetup();
        tmpJNDI.loadSetting();
        FuncDBSetup.Backup(tmpJNDI.getDataSource());
    }

    /**
     * テスト前処理。
     */
    @Before
    public void Before() {
        jndi_ = new FuncJNDISetup();
        jndi_.loadSetting();
        FuncDBSetup.Delete(jndi_.getDataSource());
        FuncDBSetup.InsertTestData(jndi_.getDataSource(), "src/test/resources/sqlTestData/DatabaseFunctionTest.xml");
    }

    /**
     * データベース復元。
     */
    @AfterClass
    public static void AfterClass() {
        FuncJNDISetup tmpJNDI = new FuncJNDISetup();
        tmpJNDI.loadSetting();
        FuncDBSetup.Restore(tmpJNDI.getDataSource());
    }

    /**
     * 正常にレコードが作成される。
     * @throws Exception エラー全般
     */
    @Test
    public void CreateRecordTest_正常にレコードが作成される() throws Exception {

        //テスト
        DatabaseFunction dbFunc = new DatabaseFunction();
        HashMap<String, String> record = null;
        try (Connection con = jndi_.getDataSource().getConnection();
                Statement stm = con.createStatement()) {
            ResultSet rs = stm.executeQuery("select *from thread where thread_id = 1");
            ResultSetMetaData rsmd = rs.getMetaData();
            rs.next();
            record = dbFunc.CreateRecord(rs, rsmd);
        }

        //チェック
        assertThat(record.size(), is(8));
        assertThat(record.get("thread_id"), is("1"));
    }

    /**
     * 不正な引数で、空のリストが返ってくる。<br>
     * 引数にnullを渡す。
     * @throws Exception エラー全般
     */
    @Test
    public void CrateReCordTest_不正な引数() throws Exception {
        //テスト
        DatabaseFunction dbFunc = new DatabaseFunction();
        HashMap<String, String> record = null;
        record = dbFunc.CreateRecord(null, null);

        //チェック
        assertThat(record.size(), is(0));
    }

    /**
     * ResultSetのカーソル位置が不正な場合、例外(SQLRuntimeExceptionが発生する。<br>
     * next()を読んでいない。
     * @throws Exception エラー全般
     */
    @Test(expected = SQLRuntimeException.class)
    public void CreateRecordTest_不正なResultSet() throws Exception {
        //テスト
        DatabaseFunction dbFunc = new DatabaseFunction();
        try (Connection con = jndi_.getDataSource().getConnection();
                Statement stm = con.createStatement()) {
            ResultSet rs = stm.executeQuery("select *from thread where thread_id = 1");
            ResultSetMetaData rsmd = rs.getMetaData();
            dbFunc.CreateRecord(rs, rsmd);
        }
    }

    /**
     * 正常に処理できた場合、レコードリストが返る。
     * @throws Exception エラー全般
     */
    @Test
    public void CreateRecordListTest_正常にレコードリストが作成される() throws Exception {
        //テスト
        DatabaseFunction dbFunc = new DatabaseFunction();
        ArrayList<HashMap<String, String>> recordList = null;
        try (Connection con = jndi_.getDataSource().getConnection();
                Statement stm = con.createStatement()) {
            ResultSet rs = stm.executeQuery("select *from thread order by thread_id");
            recordList = dbFunc.CreateRecordList(rs);
        }

        //チェック
        assertThat(recordList.size(), is(2));
        assertThat(recordList.get(0).get("thread_id"), is("1"));
        assertThat(recordList.get(1).get("thread_id"), is("2"));
    }

    /**
     * 不正な引数で空のレコードリストが返る。
     * @throws Exception エラー全般
     */
    @Test
    public void CreateRecordListTest_不正な引数() throws Exception {
        //テスト
        DatabaseFunction dbFunc = new DatabaseFunction();
        ArrayList<HashMap<String, String>> recordList = null;
        recordList = dbFunc.CreateRecordList(null);

        //チェック
        assertThat(recordList.size(), is(0));
    }
}
