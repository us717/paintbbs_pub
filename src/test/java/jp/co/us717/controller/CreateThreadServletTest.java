package jp.co.us717.controller;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jp.co.us717.testfunc.FuncDBSetup;
import jp.co.us717.testfunc.FuncJNDISetup;
import static org.mockito.Mockito.*;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.AfterClass;
import org.mockito.internal.util.reflection.Whitebox;

/**
 * CreateThreadServletのテスト。
 */
public class CreateThreadServletTest {

    /** JNDI */
    private FuncJNDISetup jndi_;

    /**
     * JNDIセットアップ、データバックアップ。
     */
    @BeforeClass
    public static void BeforeClass() {
        FuncJNDISetup.SetJNDI();
        FuncJNDISetup tmpJNDI = new FuncJNDISetup();
        tmpJNDI.loadSetting();
        FuncDBSetup.Backup(tmpJNDI.getDataSource());
    }

    /**
     * テスト前処理。
     */
    @Before
    public void Before() {
        jndi_ = new FuncJNDISetup();
        jndi_.loadSetting();
        FuncDBSetup.Delete(jndi_.getDataSource());
    }

    /**
     * データベース復元。
     */
    @AfterClass
    public static void AfterClass() {
        FuncJNDISetup tmpJNDI = new FuncJNDISetup();
        tmpJNDI.loadSetting();
        FuncDBSetup.Restore(tmpJNDI.getDataSource());
    }

    /**
     * サーブレットが正常に動作しているか。
     * @throws Exception エラー全般
     */
    @Test
    public void Get_正常に動作しているか() throws Exception {

        //モック作成
        HttpServletRequest req = mock(HttpServletRequest.class);
        HttpServletResponse res = mock(HttpServletResponse.class);
        ServletContext sc = mock(ServletContext.class);

        when(req.getParameter("width")).thenReturn("100");
        when(req.getParameter("height")).thenReturn("100");
        when(req.getParameter("threadName")).thenReturn("スレッドテスト");
        when(req.getRemoteAddr()).thenReturn("1");
        when(sc.getRealPath("/Draw")).thenReturn("src/test/resources/testimage");

        //テスト
        CreateThreadServlet servlet = new CreateThreadServlet() {
            private static final long serialVersionUID = 1L;

            public ServletContext getServletContext() {
                return sc;
            }
        };
        servlet.init();
        Whitebox.setInternalState(servlet, "dataSource_", jndi_.getDataSource());
        servlet.doPost(req, res);

        //チェック
        verify(res, times(1)).sendRedirect("null/jsp/success_thread.jsp");
    }

    /**
     * サーブレットのエラーで例外が返る。<br>
     * dateSourceが設定されていない
     * @throws Exception エラー全般
     */
    @Test(expected = NullPointerException.class)
    public void Get_処理の失敗() throws Exception {
        //テスト
        HttpServletRequest req = mock(HttpServletRequest.class);
        HttpServletResponse res = mock(HttpServletResponse.class);
        RequestDispatcher dis = mock(RequestDispatcher.class);
        ServletContext sc = mock(ServletContext.class);

        CreateThreadServlet servlet = new CreateThreadServlet() {
            private static final long serialVersionUID = 1L;

            public ServletContext getServletContext() {
                return sc;
            }
        };
        when(sc.getRequestDispatcher("/WEB-INF/jsp/headlineF.jsp")).thenReturn(dis);
        servlet.doPost(req, res);
    }

}