package jp.co.us717.controller;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jp.co.us717.testfunc.FuncDBSetup;
import jp.co.us717.testfunc.FuncJNDISetup;
import static org.mockito.Mockito.*;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.AfterClass;
import org.mockito.internal.util.reflection.Whitebox;

/**
 * ShowThreadServletのテスト。
 */
public class DeleteRequestServletTest {

    /** JNDI */
    private FuncJNDISetup jndi_;

    /**
     * JNDIセットアップ、データバックアップ。
     */
    @BeforeClass
    public static void BeforeClass() {
        FuncJNDISetup.SetJNDI();
        FuncJNDISetup tmpJNDI = new FuncJNDISetup();
        tmpJNDI.loadSetting();
        FuncDBSetup.Backup(tmpJNDI.getDataSource());
    }

    /**
     * テスト前処理。
     */
    @Before
    public void Before() {
        jndi_ = new FuncJNDISetup();
        jndi_.loadSetting();
        FuncDBSetup.Delete(jndi_.getDataSource());
        FuncDBSetup.InsertTestData(jndi_.getDataSource(), "src/test/resources/sqlTestData/DeleteRequestServletTest.xml");
    }

    /**
     * データベース復元。
     */
    @AfterClass
    public static void AfterClass() {
        FuncJNDISetup tmpJNDI = new FuncJNDISetup();
        tmpJNDI.loadSetting();
        FuncDBSetup.Restore(tmpJNDI.getDataSource());
    }

    /**
     * レス削除の成功。
     * @throws Exception エラー全般
     */
    @Test
    public void Get_レス削除の成功() throws Exception {

        //モック作成
        HttpServletRequest req = mock(HttpServletRequest.class);
        HttpServletResponse res = mock(HttpServletResponse.class);
        ServletContext sc = mock(ServletContext.class);

        when(req.getParameter("type")).thenReturn("deleteRes");
        when(req.getParameter("threadId")).thenReturn("2");
        when(req.getParameter("resNum")).thenReturn("1");
        when(req.getParameter("password")).thenReturn("0000");

        //テスト
        DeleteRequestServlet servlet = new DeleteRequestServlet() {
            private static final long serialVersionUID = 1L;

            public ServletContext getServletContext() {
                return sc;
            }
        };
        servlet.init();
        Whitebox.setInternalState(servlet, "dataSource_", jndi_.getDataSource());
        servlet.doPost(req, res);

        //チェック
        verify(res, times(1)).sendRedirect("null/jsp/success_delete.jsp");
    }

    /**
     * レス削除の失敗。
     * @throws Exception エラー全般
     */
    @Test
    public void Get_レス削除の失敗() throws Exception {

        //モック作成
        HttpServletRequest req = mock(HttpServletRequest.class);
        HttpServletResponse res = mock(HttpServletResponse.class);
        ServletContext sc = mock(ServletContext.class);

        when(req.getParameter("type")).thenReturn("deleteRes");
        when(req.getParameter("threadId")).thenReturn("2");
        when(req.getParameter("resNum")).thenReturn("1");
        when(req.getParameter("delPassword")).thenReturn("000");

        //テスト
        DeleteRequestServlet servlet = new DeleteRequestServlet() {
            private static final long serialVersionUID = 1L;

            public ServletContext getServletContext() {
                return sc;
            }
        };
        servlet.init();
        Whitebox.setInternalState(servlet, "dataSource_", jndi_.getDataSource());
        servlet.doPost(req, res);

        //チェック
        verify(res, times(1)).sendRedirect("null/jsp/error_delete.jsp");
    }

    /**
     * 削除依頼の成功。
     * @throws Exception エラー全般
     */
    @Test
    public void Get_削除依頼の成功() throws Exception {

        //モック作成
        HttpServletRequest req = mock(HttpServletRequest.class);
        HttpServletResponse res = mock(HttpServletResponse.class);
        ServletContext sc = mock(ServletContext.class);

        when(req.getParameter("type")).thenReturn("deleteRequest");
        when(req.getParameter("threadId")).thenReturn("2");
        when(req.getParameter("resNum")).thenReturn("1");
        when(req.getParameter("reason")).thenReturn("1");
        when(req.getRemoteAddr()).thenReturn("1");

        //テスト
        DeleteRequestServlet servlet = new DeleteRequestServlet() {
            private static final long serialVersionUID = 1L;

            public ServletContext getServletContext() {
                return sc;
            }
        };
        servlet.init();
        Whitebox.setInternalState(servlet, "dataSource_", jndi_.getDataSource());
        servlet.doPost(req, res);

        //チェック
        verify(res, times(1)).sendRedirect("null/jsp/acceptance_delete.jsp");
    }

    /**
     * 削除依頼の失敗。
     * @throws Exception エラー全般
     */
    @Test(expected = RuntimeException.class)
    public void Get_削除依頼の失敗() throws Exception {

        //モック作成
        HttpServletRequest req = mock(HttpServletRequest.class);
        HttpServletResponse res = mock(HttpServletResponse.class);
        ServletContext sc = mock(ServletContext.class);

        when(req.getParameter("type")).thenReturn("deleteRequest");
        when(req.getParameter("threadId")).thenReturn("2");
        when(req.getParameter("resNum")).thenReturn("1");
        when(req.getParameter("reason")).thenReturn("100");

        //テスト
        DeleteRequestServlet servlet = new DeleteRequestServlet() {
            private static final long serialVersionUID = 1L;

            public ServletContext getServletContext() {
                return sc;
            }
        };
        servlet.init();
        Whitebox.setInternalState(servlet, "dataSource_", jndi_.getDataSource());
        servlet.doPost(req, res);
    }

}