package jp.co.us717.controller;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jp.co.us717.testfunc.FuncDBSetup;
import jp.co.us717.testfunc.FuncJNDISetup;
import static org.mockito.Mockito.*;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.AfterClass;
import org.mockito.internal.util.reflection.Whitebox;

/**
 * PostResServletのテスト。
 */
public class PostResServletTest {

    /** JNDI */
    private FuncJNDISetup jndi_;

    /**
     * JNDIセットアップ、データバックアップ。
     */
    @BeforeClass
    public static void BeforeClass() {
        FuncJNDISetup.SetJNDI();
        FuncJNDISetup tmpJNDI = new FuncJNDISetup();
        tmpJNDI.loadSetting();
        FuncDBSetup.Backup(tmpJNDI.getDataSource());
    }

    /**
     * テスト前処理。
     */
    @Before
    public void Before() {
        jndi_ = new FuncJNDISetup();
        jndi_.loadSetting();
        FuncDBSetup.Delete(jndi_.getDataSource());
        FuncDBSetup.InsertTestData(jndi_.getDataSource(), "src/test/resources/sqlTestData/PostResServletTest.xml");
    }

    /**
     * データベース復元。
     */
    @AfterClass
    public static void AfterClass() {
        FuncJNDISetup tmpJNDI = new FuncJNDISetup();
        tmpJNDI.loadSetting();
        FuncDBSetup.Restore(tmpJNDI.getDataSource());
    }

    /**
     * サーブレットが正常に動作しているか。
     * @throws Exception エラー全般
     */
    @Test
    public void Get_正常に動作しているか() throws Exception {

        //テスト用画像データ作成
        BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(
                "src/test/resources/testPaintDate.txt")));
        StringBuilder sb = new StringBuilder();
        String line;
        while ((line = reader.readLine()) != null) {
            sb.append(line);
        }
        reader.close();

        //モック作成
        HttpServletRequest req = mock(HttpServletRequest.class);
        HttpServletResponse res = mock(HttpServletResponse.class);
        ServletContext sc = mock(ServletContext.class);

        when(sc.getRealPath("/Draw")).thenReturn("src/test/resources/testimage");
        when(req.getRemoteAddr()).thenReturn("1");
        when(req.getParameter("id")).thenReturn("1");
        when(req.getParameter("paintData")).thenReturn(sb.toString());
        when(req.getParameter("password")).thenReturn("1");
        when(req.getParameter("poster")).thenReturn("1");

        //テスト
        PostResServlet servlet = new PostResServlet() {
            private static final long serialVersionUID = 1L;

            public ServletContext getServletContext() {
                return sc;
            }
        };
        servlet.init();
        Whitebox.setInternalState(servlet, "dataSource_", jndi_.getDataSource());
        servlet.doPost(req, res);

        //チェック
        verify(res, times(1)).sendRedirect("null/jsp/success_res.jsp?id=1");
    }

    /**
     * サーブレットのエラーで例外が返る。<br>
     * dateSourceが設定されていない
     * @throws Exception エラー全般
     */
    @Test(expected = NullPointerException.class)
    public void Get_処理の失敗() throws Exception {
        //テスト
        HttpServletRequest req = mock(HttpServletRequest.class);
        HttpServletResponse res = mock(HttpServletResponse.class);
        RequestDispatcher dis = mock(RequestDispatcher.class);
        ServletContext sc = mock(ServletContext.class);

        PostResServlet postRes = new PostResServlet() {
            private static final long serialVersionUID = 1L;

            public ServletContext getServletContext() {
                return sc;
            }
        };
        when(sc.getRequestDispatcher("/WEB-INF/jsp/headlineF.jsp")).thenReturn(dis);
        postRes.doPost(req, res);
    }

}