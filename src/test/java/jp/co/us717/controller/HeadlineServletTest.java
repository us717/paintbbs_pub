package jp.co.us717.controller;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jp.co.us717.testfunc.FuncDBSetup;
import jp.co.us717.testfunc.FuncJNDISetup;
import static org.mockito.Mockito.*;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.AfterClass;
import org.mockito.internal.util.reflection.Whitebox;

/**
 * CreateThreadBeanのテスト。
 */
public class HeadlineServletTest {

    /** JNDI */
    private FuncJNDISetup jndi_;

    /**
     * JNDIセットアップ、データバックアップ。
     */
    @BeforeClass
    public static void BeforeClass() {
        FuncJNDISetup.SetJNDI();
        FuncJNDISetup tmpJNDI = new FuncJNDISetup();
        tmpJNDI.loadSetting();
        FuncDBSetup.Backup(tmpJNDI.getDataSource());
    }

    /**
     * テスト前処理。
     */
    @Before
    public void Before() {
        jndi_ = new FuncJNDISetup();
        jndi_.loadSetting();
        FuncDBSetup.Delete(jndi_.getDataSource());
        FuncDBSetup.InsertTestData(jndi_.getDataSource(), "src/test/resources/sqlTestData/HeadlineServletTest.xml");
    }

    /**
     * データベース復元。
     */
    @AfterClass
    public static void AfterClass() {
        FuncJNDISetup tmpJNDI = new FuncJNDISetup();
        tmpJNDI.loadSetting();
        FuncDBSetup.Restore(tmpJNDI.getDataSource());
    }

    /**
     * サーブレットが正常に動作しているか。
     * @throws Exception エラー全般
     */
    @Test
    public void Get_正常に動作しているか() throws Exception {
        //モック作成
        HttpServletRequest req = mock(HttpServletRequest.class);
        HttpServletResponse res = mock(HttpServletResponse.class);
        RequestDispatcher dis = mock(RequestDispatcher.class);
        ServletContext sc = mock(ServletContext.class);

        HeadlineServlet headline = new HeadlineServlet() {
            private static final long serialVersionUID = 1L;

            public ServletContext getServletContext() {
                return sc;
            }
        };

        //テスト
        when(sc.getRequestDispatcher("/WEB-INF/jsp/headlineF.jsp")).thenReturn(dis);
        headline.init();
        Whitebox.setInternalState(headline, "dataSource_", jndi_.getDataSource());
        headline.doGet(req, res);

        //チェック
        verify(sc, times(1)).getRequestDispatcher(anyString());
        verify(dis, times(1)).forward(req, res);
    }

    /**
     * サーブレットのエラーで例外が返る。<br>
     * dateSourceが設定されていない
     * @throws Exception エラー全般
     */
    @Test(expected = NullPointerException.class)
    public void Get_処理の失敗() throws Exception {
        //テスト
        HttpServletRequest req = mock(HttpServletRequest.class);
        HttpServletResponse res = mock(HttpServletResponse.class);
        RequestDispatcher dis = mock(RequestDispatcher.class);
        ServletContext sc = mock(ServletContext.class);

        HeadlineServlet headline = new HeadlineServlet() {
            private static final long serialVersionUID = 1L;

            public ServletContext getServletContext() {
                return sc;
            }
        };
        when(sc.getRequestDispatcher("/WEB-INF/jsp/headlineF.jsp")).thenReturn(dis);
        headline.doGet(req, res);
    }

}