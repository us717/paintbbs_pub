<%@ page contentType="text/html; charset=UTF-8" errorPage="/WEB-INF/jsp/error.jsp"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8" />
<link rel="shortcut icon" href="<%=request.getContextPath()%>/favicon.ico">
<link rel="stylesheet" href="<%=request.getContextPath()%>/css/headline.css" type="text/css"/>
<script src="<%=request.getContextPath()%>/js/delete.js"></script>
<title>お絵かき掲示板SHIGEMI</title>
</head>

<body>
    <div id="title">
    <h1>お絵かき掲示板(β)</h1>
    <h1>SHIGEMI</h1>
    <img src="<%=request.getContextPath()%>/Draw/shigemi.jpg" width="150px">
    </div>

    <table id="info">
    <tr>
        <td><a href="<%=request.getContextPath()%>/jsp/headline.jsp">トップ</a></td>
        <td><a href="<%=request.getContextPath()%>/jsp/faq.jsp">使い方</a></td>
        <td><a href="<%=request.getContextPath()%>/jsp/info.jsp">お知らせ</a></td>
        <td><a href="<%=request.getContextPath()%>/jsp/createthread.jsp">スレッド作成</a></td>
    </tr>
    </table>

    <div id="threadList">
        <table>
        <c:forEach var="table" items="${threadTable}" varStatus="status"  >
            <tr>
                <td>
                <c:out value="${status.count}." />
                </td>
                <td>
                <a href="<%=request.getContextPath()%><c:out value="/jsp/thread.jsp?thread=${table.threadId}" />">
                <c:out value="${table.threadName}"/>&nbsp;<c:out value="(${table.resCount})" />
                </a>
                </td>
            <tr>
        </c:forEach>
        </table>
    </div><hr>

    <%-- スレッド表示 --%>
    <c:forEach var="thread" items="${headlineThread}">
        <div id="res">
        <h3><c:out value="${thread.threadName}" /> </h3>

        <%-- 各スレッドの新着レス表示 --%>
        <c:if test="${thread.resCount>0}">
        <div>
            <c:forEach var="res" items="${headlineRes[thread.threadId]}">
                <c:if test="${res.show}">
                <dl>
                <dt>
                    ${res.resNum}:
                    <c:out value="${res.poster}"/>:
                    <fmt:parseDate var="result" value="${res.postDate}" pattern="yyyy-MM-dd HH:mm:ss" />
                    <span><fmt:formatDate value="${result}" pattern="yyyy/MM/dd(E) HH:mm:ss" /></span>
                    <a href="<%=request.getContextPath()%>/jsp/delete_nonejs.jsp?thread=${thread.threadId}"
                     onClick="openWnd('<%=request.getContextPath()%>/jsp/delete.jsp?thread=${thread.threadId}&num=${res.resNum}'); return false;">削除</a>
                    <a target="_blank"
                    href="<%=request.getContextPath()%><c:out value="/Draw/${res.imagePath}" />">
                     別ページ
                    </a>
                </dt>
                <dd>
                    <div id="resImage">
                    <img class="resImage" src="<%=request.getContextPath()%><c:out value="/Draw/${res.imagePath}" />">
                    </div>
                </dd>
                </dl>
                </c:if>
                <c:if test="${!res.show}">
                <dl>
                <dt>
                    ${res.resNum}:
                    <c:out value="${res.poster}"/>
                    <fmt:parseDate var="result" value="${res.postDate}" pattern="yyyy-MM-dd HH:mm:ss" />
                    <span><fmt:formatDate value="${result}" pattern="yyyy/MM/dd(E) HH:mm:ss" /></span>
                </dt>
                <dd>
                    <img src="<%=request.getContextPath()%><c:out value="/Draw/delete.jpg" />">
                </dd>
                </dl>
                </c:if>
            </c:forEach>
        </div>
        </c:if>

        <div id="threadLink">
            <a href="<%=request.getContextPath()%><c:out value="/jsp/thread.jsp?thread=${thread.threadId}" />">全て表示</a>
        </div>
        </div>
    </c:forEach><hr>

    <%-- ページ移動 --%>
    <div id="pageMove">
        <form method="get" action="<%=request.getContextPath()%>/jsp/headline.jsp">
        <table>
            <tr>
            <%-- スレッドがあるかチェック --%>
            <c:if var="isThread" test="${threadTable.size()>0}" />
            <c:if test="${isThread}">

                <%-- 前のページ --%>
                <c:if var="isPrePage" test="${page!=0}" />
                <c:if test="${isPrePage}">
                    <th><button type="submit" name="page" value="<c:out value="${page-1}"/>">前のページ</button></th>
                </c:if>
                <c:if test="${!isPrePage}">
                    <th>前のページ</th>
                </c:if>

                <%-- ページ移動 --%>
                <c:set var="threadSize" value="${threadTable.size()-1}" />
                <c:set var="totalPage" value="${threadSize/articleNum}" />
                <c:forEach begin="0" end="${totalPage}" varStatus="status">
                    <c:if test="${page==status.index}" var="isCurrentPage" />
                    <c:if test="${!isCurrentPage}">
                        <th>
                            <a class="nonVisit" href="<%=request.getContextPath()%>/jsp/headline.jsp?page=${status.index}">
                            <c:out value="[${status.index}]" /></a>
                        </th>
                    </c:if>
                    <c:if test="${isCurrentPage}">
                        <th><c:out value="[${status.index}]" /></th>
                    </c:if>
                </c:forEach>

                <%-- 次のページ --%>
                <c:if var="isNextPage" test="${(page+1)*articleNum<threadTable.size()}" />
                <c:if test="${isNextPage}">
                    <th><button type="submit" name="page" value="<c:out value="${page+1}"/>">次のページ</button></th>
                </c:if>
                <c:if test="${!isNextPage}">
                    <th>次のページ</th>
                </c:if>
            </c:if>

            <c:if test="${!isThread}">
                <th>[0]</th>
            </c:if>
           </tr>
        </table>
        </form>
    </div>
</body>

<footer>
    <p>Copyright© 2015 PaintShigemi All Rights Reserved</p>
</footer>

</html>