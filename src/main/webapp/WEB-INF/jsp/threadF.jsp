<%@ page contentType="text/html; charset=UTF-8" errorPage="/WEB-INF/jsp/error.jsp"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8" />
<link rel="shortcut icon" href="<%=request.getContextPath()%>/favicon.ico">
<link rel="stylesheet" href="<%=request.getContextPath()%>/lib/jquery.simple-color-picker.css" />
<link rel="stylesheet" href="<%=request.getContextPath()%>/lib/ui/jquery-ui.min.css" />
<link rel="stylesheet" href="<%=request.getContextPath()%>/css/headline.css" type="text/css"/>
<link rel="stylesheet" href="<%=request.getContextPath()%>/css/thread.css" type="text/css"/>
<script src="<%=request.getContextPath()%>/lib/jquery-2.1.1.min.js"></script>
<script src="<%=request.getContextPath()%>/lib/jquery.validate.min.js"></script>
<script src="<%=request.getContextPath()%>/lib/validate-custom.js"></script>
<script src="<%=request.getContextPath()%>/lib/messages_ja.min.js"></script>
<script src="<%=request.getContextPath()%>/lib/jquery.simple-color-picker.js"></script>
<script src="<%=request.getContextPath()%>/lib/ui/jquery-ui.min.js"></script>
<script src="<%=request.getContextPath()%>/js/thread.js"></script>
<script src="<%=request.getContextPath()%>/js/delete.js"></script>
<title> <c:out value="${threadInfo.threadName}" /></title>
</head>

<body>
    <div id="returnTop">
        <a href="<%=request.getContextPath()%>/jsp/headline.jsp">掲示板に戻る</a>
    </div><hr>
    <%-- スレッド表示 --%>
    <div id="res">
    <h3><c:out value="${threadInfo.threadName}" /></h3>

    <%-- レス表示 --%>
    <c:forEach var="res" items="${resList}">
        <dl>
        <c:if test="${res.show}">
            <dt>
                ${res.resNum}:
                <c:out value="${res.poster}"/>:
                <fmt:parseDate var="result" value="${res.postDate}" pattern="yyyy-MM-dd HH:mm:ss" />
                <span><fmt:formatDate value="${result}" pattern="yyyy/MM/dd(E) HH:mm:ss" /></span>
                <a href="<%=request.getContextPath()%>/jsp/delete_nonejs.jsp?thread=${threadInfo.threadId}"
                 onClick="openWnd('<%=request.getContextPath()%>/jsp/delete.jsp?thread=${threadInfo.threadId}&num=${res.resNum}'); return false;">削除</a>
                <a target="_blank"
                href="<%=request.getContextPath()%><c:out value="/Draw/${res.imagePath}" />">
                 別ページ
                </a>
            </dt>
            <dd>
                <img src="<%=request.getContextPath()%><c:out value="/Draw/${res.imagePath}" />">
        </c:if>
        <c:if test="${!res.show}">
            <dt>
                ${res.resNum}:
                <c:out value="${res.poster}"/>:
                <fmt:parseDate var="result" value="${res.postDate}" pattern="yyyy-MM-dd HH:mm:ss" />
                <span><fmt:formatDate value="${result}" pattern="yyyy/MM/dd(E) HH:mm:ss" /></span>
            </dt>
            <dd>
                <img src="<%=request.getContextPath()%><c:out value="/Draw/delete.jpg" />">
            </dd>
        </c:if>
        </dl>
    </c:forEach>
    </div>
    <a id="refresh" href="<%=request.getContextPath()%>/jsp/thread.jsp?thread=${threadInfo.threadId}">リロード</a>
    <hr>
    <div id="returnBottom">
        <a href="<%=request.getContextPath()%>/jsp/headline.jsp" onClick=location>掲示板に戻る</a>
    </div>

    <form id="createResForm" autocomplete="off" action="<%=request.getContextPath()%>/PostResServlet" method="post">
    <input id="postRes" type="button" value="書き込む">
    名前:<input type="text" name="poster" size="23" >
    パスワード(半角):<input type="text" name="password" size="10" >
    <input type="hidden" name="id" value="${threadInfo.threadId}">
    <input id="paintData" type="hidden" name="paintData">
    </form>

    <canvas
         id="paint"
         width="<c:out value="${threadInfo.width}"/>"
         height="<c:out value="${threadInfo.height}"/>" >
         このブラウザには対応していません。
    </canvas>

    <div id="paintFunc">
        <button id="resetButton">削除</button>
        <button id="undoButton">やり直し</button>
        <input  id="paintColor" type="text" readonly="readonly" size="8" placeholder="#000000"/>
        <div id="funcSlider">
            <div id="sliderLine"></div>
            <div id="sliderAlpha"></div>
        </div>
    </div>
</body>

<footer>
    <p>Copyright© 2015 PaintShigemi All Rights Reserved</p>
</footer>
</html>