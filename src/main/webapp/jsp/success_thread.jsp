<%@ page contentType="text/html; charset=UTF-8" errorPage="/WEB-INF/jsp/error.jsp"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8" />
<link rel="shortcut icon" href="<%=request.getContextPath()%>/favicon.ico">
<c:set var="context" value="<%=request.getContextPath()%>"/>
<meta http-equiv="refresh" content="3;url=<c:out value="${context}/jsp/headline.jsp"/>" />
<title>お絵かき掲示板SHIGEI</title>
</head>
<body>
    <p>スレッドの作成に成功しました。</p>
    <p>画面を切り替えるまでしばらくお待ちください。</p>
</body>
</html>