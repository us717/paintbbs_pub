<%@ page contentType="text/html; charset=UTF-8" errorPage="/WEB-INF/jsp/error.jsp"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8" />
<link rel="shortcut icon" href="<%=request.getContextPath()%>/favicon.ico">
<style>
    body{text-align:center;}
    #title{margin-top:50px;}
</style>
<title>削除・削除依頼</title>
</head>
<body>
    <p id="title">削除依頼を受け付けました。</p>
    <button type="button" onclick="window.close()">閉じる</button>
</body>

</html>