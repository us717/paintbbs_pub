<%@ page contentType="text/html; charset=UTF-8" errorPage="/WEB-INF/jsp/error.jsp"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8" />
<link rel="shortcut icon" href="<%=request.getContextPath()%>/favicon.ico">
<link rel="stylesheet" href="<%=request.getContextPath()%>/css/delete.css" type="text/css"/>
<script src="<%=request.getContextPath()%>/lib/jquery-2.1.1.min.js"></script>
<script src="<%=request.getContextPath()%>/lib/jquery.validate.min.js"></script>
<script src="<%=request.getContextPath()%>/lib/validate-custom.js"></script>
<script src="<%=request.getContextPath()%>/lib/messages_ja.min.js"></script>
<script src="<%=request.getContextPath()%>/js/delete.js"></script>
<title>削除・削除依頼</title>
</head>

<body>
    <h3 id="title">削除・削除依頼</h3>
    <div id="info">
        <p>スレッドID:<c:out value="${param.thread}"/> </p>
        <p>レス番号:<c:out value="${param.num}"/> </p>
    </div>
    <form id="delRes" autocomplete="off" action="<%=request.getContextPath()%>/DeleteRequestServlet" method="post">
        <h4 class="type"><input id="typeDel" type="radio" name="type" value="deleteRes" >削除</h4>
        <p class="message">パスワード:<input id="delPassword" type="text" name="delPassword" size="10" ></p>
        <h4 class="type"><input id="typeReq" type="radio" name="type" value="deleteRequest">削除依頼</h4>
        <p class="message"><input class="req" id="reqInitVal" type="radio" name="reason" value="0">荒らし</p>
        <p class="message"><input class="req" type="radio" name="reason" value="1">誹謗中傷</p>
        <p class="message"><input class="req" type="radio" name="reason" value="2">プライバシーの侵害</p>
        <p class="message"><input class="req" type="radio" name="reason" value="3">スパム</p>
        <p class="message"><input class="req" type="radio" name="reason" value="4">犯罪予告、犯罪行為</p>
        <input type="hidden" name="threadId" value="${param.thread}">
        <input type="hidden" name="resNum" value="${param.num}">
        <input id="postDelRes" type="button" value="送信する" >
    </form>
</body>
<footer>
    <p>Copyright© 2015 PaintShigemi All Rights Reserved</p>
</footer>
</html>