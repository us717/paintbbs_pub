<%@ page contentType="text/html; charset=UTF-8" errorPage="/WEB-INF/jsp/error.jsp"%>
<%@ page trimDirectiveWhitespaces="true"%>
<!DOCTYPE html>

<html>
<head>
<meta charset="UTF-8" />
<meta name="description" content="お絵かき掲示板SHIGEMI">
<meta name="keywords" content="お絵かき掲示板SHIGEMI,お絵かき掲示板,SHIGEMI,higemi,paintbbs,掲示板,お絵かき">

<link rel="shortcut icon" href="<%=request.getContextPath()%>/favicon.ico">
<style>
    #title{text-align:center;}
    img{border: thin dotted }
    #entrance{font-size:30px;}
    footer{text-align:right;}
</style>
<title>お絵かき掲示板SHIGEMI</title>
</head>
<body>
    <div id="title">
    <h1>お絵かき掲示板(β)</h1>
    <h1>SHIGEMI</h1>
    <a href="<%=request.getContextPath()%>/jsp/headline.jsp"><img src="<%=request.getContextPath()%>/Draw/shigemi.jpg" alt="入口"></a>
    <div>
    <a id="entrance" href="<%=request.getContextPath()%>/jsp/headline.jsp">入口</a>
    </div>
    </div>
</body>

<footer>
    <p>Copyright© 2015 PaintShigemi All Rights Reserved</p>
</footer>
</html>