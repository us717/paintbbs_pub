<%@ page contentType="text/html; charset=UTF-8" errorPage="/WEB-INF/jsp/error.jsp"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8" />
<link rel="shortcut icon" href="<%=request.getContextPath()%>/favicon.ico">
<style>
    body{margin-topsub:30px; margin-bottom:40px; margin-left:60px; margin-right:60px;}
    .log{margin-left:30px;}
    footer{text-align:right;}
</style>
<title>お絵かき掲示板SHIGEMI</title>
</head>
<body>
    <div>
        <a href="<%=request.getContextPath()%>/jsp/headline.jsp">掲示板に戻る</a>
    </div><hr>

    <h3 class="sub">お知らせ</h3>
    <table class="log">
        <tr>
            <td>2015年2月9日～</td>
            <td>現在テスト運営中です。</td>
        </tr>
    </table>
    <h3 class="sub">更新履歴</h3>
    <table class="log">
        <tr>
            <td>2015年2月9日</td>
            <td>サービス(β)開始</td>
        </tr>
    </table>
    <hr>
    <div>
        <a href="<%=request.getContextPath()%>/jsp/headline.jsp">掲示板に戻る</a>
    </div>
</body>

<footer>
    <p>Copyright© 2015 PaintShigemi All Rights Reserved</p>
</footer>
</html>