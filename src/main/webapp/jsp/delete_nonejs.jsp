<%@ page contentType="text/html; charset=UTF-8" errorPage="/WEB-INF/jsp/error.jsp"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8" />
<link rel="shortcut icon" href="<%=request.getContextPath()%>/favicon.ico">
<c:set var="context" value="<%=request.getContextPath()%>"/>
<meta http-equiv="refresh" content="3;url=<c:out value="${context}/jsp/thread.jsp?thread=${param.thread}"/>" />
<title>削除・削除依頼</title>
</head>
<body>
    <p>JavaScriptが無効になっているため、利用できません。</p>
    <p>JavaScriptの設定を有効にして下さい。</p>
<body>
</html>