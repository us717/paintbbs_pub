<%@ page contentType="text/html; charset=UTF-8" errorPage="/WEB-INF/jsp/error.jsp"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8" />
<link rel="shortcut icon" href="<%=request.getContextPath()%>/favicon.ico">
<style>
    body{margin-top:30px; margin-bottom:40px; margin-left:60px; margin-right:60px;}
    #title{margin-top:50px;}
    .sub{margin-bottom:5px;}
    .list{margin-top:5px;}
    #mail{ margin-left:10px;margin-top:2px;}
    footer{text-align:right;}
</style>
<title>お絵かき掲示板SHIGEMI</title>
</head>
<body>
    <div>
        <a href="<%=request.getContextPath()%>/jsp/headline.jsp">掲示板に戻る</a>
    </div>
    <hr>
    <h3 class="sub" >&lt;サイトについて&gt;</h3>
    <ol class="list">
        <li>本サイトご利用いただくにはJavaScriptを有効にする必要があります。JavaScriptが無効となっている場合、スレッド作成、レス投稿削除などの一部機能がご利用できなくなります。</li>
        <li>本サイトはInternetExplorer8以下のブラウザには対応していません。</li>
        <li>保持スレッド数は最大30件、保持件数を超えると更新の古いものから順次削除されていきます。</li>
        <li>1スレッド最大100件までレスの投稿が可能です。</li>
        <li>本サイトの投稿について、不適切なものと判断されるものは、管理人判断で予告なく削除します。</li>
        <li>本サイトの運営は予告なく終了、削除される場合があります。</li>
    </ol>

    <h3 class="sub">&lt;投稿について&gt;</h3>
    <ol class="list">
        <li>投稿内容は全て投稿者自身が責任を持つこととします。投稿内容によって生じた問題に関して、本サイト管理者は一切の責任を負いません。</li>
        <li>投稿者は下記禁則事項を守り、常識の範囲内での書き込みを行ってください。</li>
    </ol>

    <h3 class="sub">&lt;禁則事項&gt;</h3>
    <ol class="list">
        <li>荒らし行為</li>
        <li>誹謗中傷</li>
        <li>プライバシーや著作権を侵害する行為</li>
        <li>スパム行為</li>
        <li>犯罪予告や犯罪行為などの違法行為</li>
        <li>投稿後30秒以内の連続投稿</li>
    </ol>

    <h3 class="sub">&lt;スレッド作成&gt;</h3>
    <ol class="list">
        <li>スレッド名は必須、64字以内まで書き込むことができます。</li>
        <li>お絵かきキャンバスのサイズは最少100×100、最大600×300の範囲で指定できます。</li>
    </ol>

    <h3 class="sub">&lt;レス投稿&gt;</h3>
    <ol class="list">
        <li>名前の上限は32文字以下で入力は必須ではありません。書き込まなかった場合、名前はデフォルトの名前(名無し)になります。</li>
        <li>パスワードは10文字以内の半角英数字で、入力は必須ではありません。</li>
        <li>パスワードを設定することにより、削除・削除依頼ページから、該当レスを削除することが可能になります。設定を行ってない場合削除はできません。</li>
    </ol>

    <h3 class="sub">&lt;削除・削除依頼&gt;</h3>
    <ol class="list">
        <li>投稿時、パスワードを入力していた場合のみ、該当レスを削除することが可能です。</li>
        <li>削除依頼は、管理人判断で禁則事項に該当する場合のみ削除します。</li>
        <li>受付られなかった削除依頼は、お知らせにて告知するので確認してください。</li>
        <li>スレッドの削除、その他レスの削除依頼はメールにてお問い合わせ下さい。</li>
        <li>削除依頼の確認は不定期で行っていますので、反映には多少時間がかかる場合がございます。</li>
    </ol>
    <h3 class="sub">&lt;免責事項&gt;</h3>
    <ol class="list">
        <li>利用者が本サービスを利用することによって生じた苦情、損失・損害等について、管理者は損害賠償その他の責任を負いません。</li>
        <li>管理者は利用者の投稿内容を削除し、保存しなかったことについて一切責任を負わず、その理由説明義務を負いません。</li>
    </ol>
    <h3 class="sub">&lt;サポート&gt;</h3>
    <ol class="list">
        <li>管理者に連絡を取る場合はこちらのアドレスからお願いします。
        <h2 id="mail">paintbbs.shigemi@gmail.com</h2>
    </ol>
    <hr>
    <div>
        <a href="<%=request.getContextPath()%>/jsp/headline.jsp">掲示板に戻る</a>
    </div>
</body>

<footer>
    <p>Copyright© 2015 PaintShigemi All Rights Reserved</p>
</footer>
</html>