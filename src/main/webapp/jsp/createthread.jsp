<%@ page contentType="text/html; charset=UTF-8" errorPage="/WEB-INF/jsp/error.jsp"%>
<%@ page trimDirectiveWhitespaces="true"%>
<!DOCTYPE html>

<html>
<head>
<meta charset="UTF-8" />
<link rel="shortcut icon" href="<%=request.getContextPath()%>/favicon.ico">
<link rel="stylesheet" href="<%=request.getContextPath()%>/lib/jquery.simple-color-picker.css" />
<link rel="stylesheet" href="<%=request.getContextPath()%>/lib/ui/jquery-ui.min.css" />
<link rel="stylesheet" href="<%=request.getContextPath()%>/css/create_thread.css" type="text/css"/>
<script src="<%=request.getContextPath()%>/lib/jquery-2.1.1.min.js"></script>
<script src="<%=request.getContextPath()%>/lib/jquery.validate.min.js"></script>
<script src="<%=request.getContextPath()%>/lib/validate-custom.js"></script>
<script src="<%=request.getContextPath()%>/lib/messages_ja.min.js"></script>
<script src="<%=request.getContextPath()%>/lib/jquery.simple-color-picker.js"></script>
<script src="<%=request.getContextPath()%>/lib/ui/jquery-ui.min.js"></script>
<script src="<%=request.getContextPath()%>/js/create_thread.js"></script>
<title>お絵かき掲示板SHIGEMI</title>
</head>

<body>
    <h2 id="title">スレッド作成</h2>
    <form id="createThreadForm" autocomplete="off" action="<%=request.getContextPath()%>/CreateThreadServlet" method="post">
    <p id="threadName">スレッド名:<input type="text" name="threadName" size="70"></p>
    <p id="canvasSize">横:<input type="text" id="canvasWidth" name="width" size="2" value="100">&nbsp;
    高さ:<input type="text" id="canvasHeight" name="height" size="2" value="100">&nbsp;&nbsp;&nbsp;
    <input id="create" type="button" value="スレッドを作成する"> </p>
    </form>

    <canvas
         id="preview"
         width="600"
         height="300" >
         このブラウザには対応していません。
    </canvas>

    <hr>
    <div id="return">
        <a href="<%=request.getContextPath()%>/jsp/headline.jsp">掲示板に戻る</a>
    </div>

</body>

<footer>
    <p>Copyright© 2015 PaintShigemi All Rights Reserved</p>
</footer>

</html>