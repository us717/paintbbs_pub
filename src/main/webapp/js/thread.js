$(function() {
    //変数定義
    var ctx, canvas;
    var isClick = false;
    var startX = 0.0;
    var startY = 0.0;
    var strokeWidth = '1';
    var undo=[];

    //読み込み
    canvas = $('#paint')[0];
    if (!canvas || !canvas.getContext)
        return;
    ctx = canvas.getContext('2d');

    // 初期設定
    ctx.lineJoin = "round"; // 角を丸くして描画する
    ctx.lineCap = "round";
    ctx.fillStyle = 'white';
    ctx.fillRect(0, 0, $('canvas').width(), $('canvas').height());


    //描画動作
    $('#paint').mousedown(
            function(e) {
                isClick = true;
                startX = e.pageX - $(this).offset().left;
                startY = e.pageY - $(this).offset().top;
                undo.push(ctx.getImageData(0, 0, $('canvas').width(), $('canvas')
                        .height()));
    }).mouseup(function() {
        isClick = false;
    }).mousemove(function(e) {
        var x = e.pageX - $(this).offset().left;
        var y = e.pageY - $(this).offset().top;
        if (!isClick)
            return;
        ctx.beginPath();
        ctx.moveTo(startX, startY);
        ctx.lineTo(x, y);
        ctx.stroke();
        startX = x;
        startY = y;
    }).mouseleave(function() {
        isClick = false;
    })

    //色選択
    $('#paintColor').simpleColorPicker( {
        colorsPerLine : 16,
        onChangeColor : function(color) {
            $('#paintColor').val(color).css("background-color", color);
            ctx.strokeStyle = color;
        }
    })

    //線の大きさ
    $('#sliderLine').slider({
        min : 1,
        max : 50,
        step : 1,
        value : 1,
        range : "min",
        change : function(e, ui) {
            ctx.lineWidth = ui.value;
        }
    })

    //透過度
    $('#sliderAlpha').slider({
        min : 0.0,
        max : 1.0,
        step : 0.01,
        value : 1.0,
        range : 'min',
        change : function(e, ui) {
            ctx.globalAlpha = ui.value;
        }
    })

    //アンドゥ
    $('#undoButton').click(function() {
        if(undo.length != 0) {
            ctx.putImageData(undo.pop(), 0, 0);
        }else {
            ctx.globalAlpha = 1.0;
            ctx.fillRect(0, 0, $('canvas').width(), $('canvas').height());// 透明化するので削除ではなく白く塗りつぶしている
            ctx.globalAlpha = tmp;
        }
    })

    //削除
    $('#resetButton').click(function(e) {
        var tmp = ctx.globalAlpha;
        ctx.globalAlpha = 1.0;
        ctx.fillRect(0, 0, $('canvas').width(), $('canvas').height());
    })

    //画像作成送信
    $('#postRes').click(function() {
        if(!window.confirm('送信してよろしいですか？')){
            return false;
        }
        if(!$('#createResForm').valid()) {
            return false;
        }

        var postImage = canvas.toDataURL('image/jpeg');
        postImage = postImage.replace('data:image/jpeg;base64,', '');
        $('#paintData').val(postImage);

       $('#postRes').attr('disabled', true);
       $('#createResForm').submit();
    })

    //バリデーションチェック
    $('#createResForm').validate({
        rules: {
            poster: {
                maxlength: 32
            },
            password: {
                maxlength: 10,
                halfwidth: true,
            }
        }
    })

})
