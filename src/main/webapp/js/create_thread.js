$(function() {
    //変数定義
    var ctx, canvas;

    //キャンバス読み込み
    canvas = $('#preview')[0];
    if (!canvas || !canvas.getContext)
        return;
    ctx = canvas.getContext('2d');

    //描画定義
    function draw(){
        ctx.clearRect(0, 0, $('#preview').width(), $('#preview').height() );
        if($('#canvasWidth').spinner('value')>=100 && $('#canvasHeight').spinner('value') >=100) {
            ctx.strokeRect(0, 0, $('#canvasWidth').spinner('value'), $('#canvasHeight').spinner('value'));
        }
     }

    //スピナー定義
     $('#canvasWidth').spinner({
        max:$('#preview').width(),
        min:100,
        spin:function() {
            draw();
        },
        change:function() {
            draw();
        }
    })
     $('#canvasHeight').spinner({
        max:$('#preview').height(),
        min:100,
        spin:function() {
            draw();
        },
        change:function() {
            draw();
        }
    })

    //スレッド作成送信
    $('#create').click(function() {
        if(!window.confirm('送信してよろしいですか？')){
            return false;
        }
        if(!$('#createThreadForm').valid()) {
            return false;
        }
        $('#create').attr('disabled', true);
        $('#createThreadForm').submit();
    })

    //バリデーションチェック
    $('#createThreadForm').validate({
        rules: {
            threadName: {
                required: true,
                maxlength: 64,
            },
        }
    });

    //初期描画
    ctx.fillStyle = 'black';
    ctx.lineWidth = 2;
    draw();

});