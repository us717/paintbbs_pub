function openWnd(url) {
    window.open(url, 'delete', 'width=360, height=560, centerscreen=yes');
}

$(function() {

    //削除選択時
    $('#typeDel').click(function(){
        $('#delPassword').attr('disabled', false);
        $('.req').prop('checked', false);
    })

    //削除依頼選択時
    $('#typeReq').click(function(){
        $('#reqInitVal').prop('checked', true)
        $('#delPassword').attr('disabled', true);
        $('#delPassword').val('');
    })

    //リクエスト選択時
     $('.req').click(function(){
        $('#typeReq').prop('checked', true)
        $('#delPassword').attr('disabled', true);
        $('#delPassword').val('');
    })

    //送信
    $('#postDelRes').click(function(){
        if(!window.confirm('送信してよろしいですか？')){
            return false;
        }
        if(!$('#delRes').valid()) {
            return false;
        }
        $('#postDelRes').attr('disabled', true);
        $('#delRes').submit();
    })

    //バリデーションチェック
    $('#delRes').validate({
        rules: {
            delPassword: {
                maxlength: 10,
                halfwidth: true,
            }
        }
    });

    //初期処理
    $('#typeDel').prop('checked', true);
})