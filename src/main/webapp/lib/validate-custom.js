jQuery.validator.addMethod(
  "halfwidth",
   function(value, element) {
     reg = new RegExp("^[0-9a-zA-Z]+$");
     return this.optional(element) || reg.test(value);
   },
   "半角英数字で入力してください。"
 );