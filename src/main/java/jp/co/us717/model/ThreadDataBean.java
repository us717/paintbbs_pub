package jp.co.us717.model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import jp.co.us717.exception.SQLRuntimeException;
import jp.co.us717.lib.BeanCommonFunction;

/**
 * ひとつのスレッド情報と、その内容(レス)データを作成する。
 */
public class ThreadDataBean extends BeanCommonFunction {

    /** スレッド番号 */
    private int threadId_;

    /** スレッド情報 */
    private ThreadBean threadInfo_;

    /** レスリスト */
    private ArrayList<ResBean> resList_;

    /**
     * コンストラクタ
     */
    public ThreadDataBean() {
        resList_ = new ArrayList<>();
    }

    /**
     * 指定したスレッドの情報と、内容(レス)リストを作成します。
     * !スレッド番号
     * @exception SQLRuntimeException データーベース接続エラー
     */
    public void SelectThreadData() {

        //初期化
        threadInfo_ = new ThreadBean();
        resList_.clear();

        //スレデータ読み込み
        String sqlSelectThreadInfo = "select thread_id, thread_name, width, height, last_update, created_date, state "
                + "from thread where thread_id = ?";
        try (Connection con = dataSource_.getConnection();
                PreparedStatement stmSelectThreadInfo = con.prepareStatement(sqlSelectThreadInfo)) {

            //データ取得
            stmSelectThreadInfo.setInt(1, threadId_);
            ResultSet rs = stmSelectThreadInfo.executeQuery();

            //データ格納
            if (rs.next()) {
                threadInfo_.setThreadId(rs.getInt(1));
                threadInfo_.setThreadName(rs.getString(2));
                threadInfo_.setWidth(rs.getInt(3));
                threadInfo_.setHeight(rs.getInt(4));
                threadInfo_.setLastUpdate(rs.getString(5));
                threadInfo_.setCreatedDate(rs.getString(6));
                threadInfo_.setIsShow(rs.getBoolean(7));
            } else {
                return; //指定したスレッドが無ければこれ以上処理しない
            }
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        }

        //レスデータ読み込み
        String sqlSelectRes = "select thread_id, poster, filename, postdate, is_password, state "
                + "from res where thread_id = ? order by postdate asc";
        try (Connection con = dataSource_.getConnection();
                PreparedStatement stmSelectRes = con.prepareStatement(sqlSelectRes)) {

            //データ取得
            stmSelectRes.setInt(1, threadId_);
            ResultSet rs = stmSelectRes.executeQuery();

            //データ格納
            for (int i = 1; rs.next(); i++) {
                ResBean tmpRes = new ResBean();
                tmpRes.setThreadId(rs.getInt(1));
                tmpRes.setPoster(rs.getString(2));
                tmpRes.setImagePath(rs.getString(3));
                tmpRes.setPostDate(rs.getString(4));
                tmpRes.setIsPassword(rs.getBoolean(5));
                tmpRes.setIsShow(rs.getBoolean(6));
                tmpRes.setResNum(i);
                resList_.add(tmpRes);
            }
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        }
    }

    /**
     * スレッド番号の設定。
     * @param threadId スレッド番号
     */
    public void setThreadId(int threadId) {
        if (threadId <= 0) {
            throw new IllegalArgumentException("0以上を指定してください");
        }
        threadId_ = threadId;
    }

    /**
     * スレッド番号の取得。
     * @return スレッド番号
     */
    public int getThreadId() {
        return threadId_;
    }

    /**
     * スレッド情報取得。
     * @return スレッド情報
     */
    public ThreadBean getThreadInfo() {
        return threadInfo_;
    }

    /**
     * レスリストの取得。
     * @return レスリスト
     */
    public ArrayList<ResBean> getResList() {
        return resList_;
    }

}
