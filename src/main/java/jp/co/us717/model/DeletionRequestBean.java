package jp.co.us717.model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Objects;

import jp.co.us717.exception.SQLRuntimeException;
import jp.co.us717.lib.BeanCommonFunction;
import jp.co.us717.lib.ContentsTypeEnum;

/**
 * 削除依頼。
 */
public class DeletionRequestBean extends BeanCommonFunction {

    /** リクエストタイプ */
    private ContentsTypeEnum requestType_;

    /** スレッドID */
    private int threadId_;

    /** レス番号 */
    private int resNum_;

    /** 依頼理由 */
    private String reason_;

    /** IPアドレス */
    private String ipAddres_;

    /**
     * コンストラクタ
     */
    public DeletionRequestBean() {
        reason_ = "";
        ipAddres_ = "";
    }

    /**
     * レス削除依頼の登録。<br>
     * !レスタイプ・スレッドID・レス番号・理由・IP
     * @exception SQLRuntimeException データベース接続エラー
     */
    public void RegisterRequestResDelete() {
        //!DB更新
        String sqlUpdate = "insert into deletion_request( request_type, thread_id, res_num, record_date, reason, addres )"
                + "values( ?, ?, ?, now(), ?, ? )";
        try (Connection con = dataSource_.getConnection();
                PreparedStatement stmUpdate = con.prepareStatement(sqlUpdate)) {
            stmUpdate.setString(1, requestType_.toString());
            stmUpdate.setInt(2, threadId_);
            stmUpdate.setInt(3, resNum_);
            stmUpdate.setString(4, reason_);
            stmUpdate.setString(5, ipAddres_);
            stmUpdate.execute();
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        }
    }

    /**
     * リクエストタイプの設定。
     * @param requestType リクエストタイプ
     */
    public void setRequestType(ContentsTypeEnum requestType) {
        requestType_ = requestType;
    }

    /*
     * スレッドIDの設定。
     * @param threadId
     */
    public void setThreadId(int threadId) {
        threadId_ = threadId;
    }

    /**
     * レス番号の設定。
     * @param resNum レス番号
     */
    public void setThreadNum(int resNum) {
        resNum_ = resNum;
    }

    /**
     * 依頼理由の設定。
     * @param reason 依頼理由
     */
    public void setReason(String reason) {
        reason_ = Objects.requireNonNull(reason);
    }

    /**
     * 依頼理由の設定。
     * @param messageTemplateNum 依頼理由
     */
    public void setReason(int messageTemplateNum) {
        if (messageTemplateNum < 0 || messageTemplateNum > 5) {
            throw new IllegalArgumentException("不正な番号");
        }

        switch (messageTemplateNum) {
        case 0:
            reason_ = "荒らし";
            break;
        case 1:
            reason_ = "誹謗中傷";
            break;
        case 2:
            reason_ = "プライバシーの侵害";
            break;
        case 3:
            reason_ = "スパム";
            break;
        case 4:
            reason_ = "犯罪予告:犯罪行為";
            break;
        }
    }

    /**
     * IPアドレスの設定。
     * @param ipAddres IPアドレス
     */
    public void setIpAddres(String ipAddres) {
        ipAddres_ = Objects.requireNonNull(ipAddres);
    }
}
