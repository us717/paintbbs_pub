package jp.co.us717.model;

import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.channels.FileLock;
import java.nio.file.Files;
import java.nio.file.StandardOpenOption;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Base64;
import java.util.Date;
import java.util.Objects;
import java.util.Random;

import jp.co.us717.exception.GeneralErrorException;
import jp.co.us717.exception.IORuntimeException;
import jp.co.us717.exception.SQLRuntimeException;
import jp.co.us717.lib.BeanCommonFunction;

import org.apache.commons.lang3.StringUtils;

/**
 * レス。<br>
 */
public class ResBean extends BeanCommonFunction {

    /** スレッドID */
    private int threadId_;

    /** レス番号 */
    private int resNum_;

    /** 投稿者の名前 */
    private String poster_;

    /** ファイル名 */
    private String filename_;

    /** 投稿日 */
    private String postDate_;

    /** パスワード */
    private String password_;

    /** パスワードの有無*/
    private boolean isPassword_;

    /** 画像データ */
    private String postImage_;

    /** 画像パス */
    private String imagePath_;

    /** 状態 */
    private boolean isShow_;

    /** IPアドレス */
    private String addres_;

    /**
     * コンストラクタ。
     */
    public ResBean() {
        poster_ = "名無し";
        filename_ = "";
        postDate_ = "";
        password_ = "0000";
        postImage_ = "";
        imagePath_ = "";
        addres_ = "";
    }

    /**
     * レスを投稿する。<br>
     * !スレッドID・投稿者・ファイル名・パスワード
     * @throws SQLRuntimeException レスの投稿に失敗した場合
     * @throws IORuntimeException  画像データの保存に失敗した場合
     */
    public void InsertRes() {

        //!ファイル名を作成する（現時刻+乱数)
        Date curDate = new Date();
        DateFormat reformDate = new SimpleDateFormat("yyMMddHHmmss");
        Random rand = new Random();
        filename_ = reformDate.format(curDate) + rand.nextInt(999) + ".jpg";

        //!画像データをデコードしてディレクトリに保存
        File file = new File(realPath_ + "/" + filename_);
        try (FileChannel fc = FileChannel.open(file.toPath(), StandardOpenOption.CREATE_NEW, StandardOpenOption.WRITE);
                FileLock lock = fc.tryLock()) {
            if (lock == null) {
                throw new GeneralErrorException();
            }
            byte[] encodedData = Base64.getDecoder().decode(postImage_);
            ByteBuffer buf = ByteBuffer.wrap(encodedData);
            fc.write(buf);
        } catch (IOException e) {
            throw new IORuntimeException(e);
        }

        //!SQL定義
        //====レスの新規作成========
        String sqlRes = "insert into res(thread_id, poster, filename, postdate, is_password, password, addres )"
                + "values(?, ?, ?, now(), ?, ?, ?)";
        //====スレッドの最終更新日更新,レス数 ======
        String sqlThread = "update thread set last_update = now(), res_count = res_count + 1 where thread_id = ? and state = true";

        //!DB更新
        try (Connection con = dataSource_.getConnection();
                PreparedStatement stmRes = con.prepareStatement(sqlRes);
                PreparedStatement stmThread = con.prepareStatement(sqlThread)) {
            try {
                //トランザクション開始
                con.setAutoCommit(false);

                //レス新規作成
                stmRes.setInt(1, threadId_);
                stmRes.setString(2, poster_);
                stmRes.setString(3, filename_);
                stmRes.setBoolean(4, isPassword_);
                stmRes.setString(5, password_);
                stmRes.setString(6, addres_);
                stmRes.executeUpdate();

                //threadの最終更新日、レス数を更新する
                stmThread.setInt(1, threadId_);
                if (stmThread.executeUpdate() != 1) { //スレッドが書き込み禁止だった場合中止
                    throw new SQLException("ERROR:スレッドが書き込み禁止状態です。スレッド" + threadId_);
                }

                //コミット
                con.commit();

            } catch (SQLException ex) {
                con.rollback();
                throw ex;
            } finally {
                con.setAutoCommit(true);
            }
        } catch (SQLException e) {
            //削除
            try {
                Files.deleteIfExists(file.toPath());//失敗したので、既に保存していた画像を削除する
            } catch (IOException ex) {
                throw new IORuntimeException(ex);
            }
            throw new SQLRuntimeException(e);
        }
    }

    /**
     * レスを表示禁止にする。<br>
     * !スレッドID・レス番号・パスワード
     * @throws SQLRuntimeException レスの書き込み禁止に失敗した場合
     */
    public void ProhibitionOfResShowing() {
        String sqlUpdate = "update res set state = false where filename in(select filename from res "
                + " where thread_id = ? order by postdate asc offset ? limit 1 ) and is_password = true and"
                + " password = ?";
        try (Connection con = dataSource_.getConnection();
                PreparedStatement stmGetDeleteFileName = con.prepareStatement(sqlUpdate)) {

            //データ取得
            stmGetDeleteFileName.setInt(1, threadId_);
            stmGetDeleteFileName.setInt(2, resNum_ - 1);
            stmGetDeleteFileName.setString(3, password_);
            if (stmGetDeleteFileName.executeUpdate() != 1) {
                throw new SQLException("書き込み禁止に失敗しました");
            }
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        }
    }

    /**
     * レス番号の設定。
     * @param resNum レス番号
     */
    public void setResNum(int resNum) {
        resNum_ = resNum;
    }

    /**
     * スレッドIDの設定。
     * @param threadId 投稿するDB内のスレッドID
     */
    public void setThreadId(int threadId) {
        if (threadId <= 0) {
            throw new IllegalArgumentException("0以上を指定してください");
        }
        threadId_ = threadId;
    }

    /**
     * 投稿者の名前設定。<br>
     * 任意。<br>
     * 空文字、NULL、スペースを設定した場合はデフォルトの名前で設定は反映されません。
     * @param poster 投稿者の名前
     */
    public void setPoster(String poster) {
        if (!StringUtils.isBlank(poster)) {
            poster_ = poster.trim();
        } else {
            poster_ = "名無し"; //デフォルトの投稿者名
        }
    }

    /**
     * 投稿日の設定。
     * @param postDate 投稿日
     */
    public void setPostDate(String postDate) {
        postDate_ = Objects.requireNonNull(postDate);
    }

    /**
     * パスワードの設定。<br>
     * 任意<br>
     * 半角英数字を設定して下さい。<br>
     * 半角英数字以外の場合、パスワードは設定されません。
     * @param password 削除用のパスワード(半角英数字)
     */
    public void setPassword(String password) {
        if (!StringUtils.isBlank(password) && password.matches("^[a-zA-Z0-9]+$")) {
            password_ = password.trim();
            isPassword_ = true;
        } else {
            password_ = "0000";
            isPassword_ = false;
        }
    }

    /**
     * パスワードの有無の設定。
     * @param isPassword パスワードの有無
     */
    public void setIsPassword(boolean isPassword) {
        isPassword_ = isPassword;
    }

    /**
     * 画像データの設定。
     * @param postImage デコードして保存する画像データ
     */
    public void setImageData(String postImage) {
        postImage_ = Objects.requireNonNull(postImage);
    }

    /**
     * 画像パスの設定。
     * @param imagePath 画像パス
     */
    public void setImagePath(String imagePath) {
        imagePath_ = Objects.requireNonNull(imagePath);
    }

    /**
     * 状態の設定。
     * @param isShow 状態
     */
    public void setIsShow(boolean isShow) {
        isShow_ = isShow;
    }

    /**
     * IPアドレスの設定。
     * @param addres IPアドレス
     */
    public void setAddres(String addres) {
        addres_ = Objects.requireNonNull(addres);
    }

    /**
     * レス番号の取得。
     * @return レス番号
     */
    public int getResNum() {
        return resNum_;
    }

    /**
     * スレッドIDの取得。
     * @return スレッドID
     */
    public int getThreadId() {
        return threadId_;
    }

    /**
     * 投稿者の名前取得。
     * @return 投稿者のい名前
     */
    public String getPoster() {
        return poster_;
    }

    /**
     * 投稿日取得の取得。
     * @return 投稿日
     */
    public String getPostDate() {
        return postDate_;
    }

    /**
     * パスワードの取得。
     * @return パスワード
     */
    public String getPassword() {
        return password_;
    }

    /**
     * パスワードの有無取得。
     * @return パスワードの有無
     */
    public boolean isPassword() {
        return isPassword_;
    }

    /**
     * 画像パスの取得。
     * @return 画像パス
     */
    public String getImagePath() {
        return imagePath_;
    }

    /**
     * 状態の取得。
     * @return 状態
     */
    public boolean isShow() {
        return isShow_;
    }
}