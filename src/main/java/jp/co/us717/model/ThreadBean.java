package jp.co.us717.model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Objects;

import jp.co.us717.exception.GeneralErrorException;
import jp.co.us717.exception.SQLRuntimeException;
import jp.co.us717.lib.BeanCommonFunction;

import org.apache.commons.lang3.StringUtils;

/**
 * スレッド。
 */
public class ThreadBean extends BeanCommonFunction {

    /** スレッドID */
    private int threadId_;

    /** スレッド名 */
    private String threadName_;

    /** 描画枠:横幅 */
    private int width_;

    /** 描画枠:高さ */
    private int height_;

    /**最終更新日 */
    private String lastUpdate_;

    /**作成日 */
    private String createdDate_;

    /**状態 */
    private boolean isShow_;

    /**IPアドレス */
    private String addres_;

    /**レスカウンタ*/
    private int resCount_;

    /**
     * コンストラクタ
     */
    public ThreadBean() {
        threadName_ = "";
        lastUpdate_ = "";
        createdDate_ = "";
        addres_ ="";
    }

    /**
     * スレッドの作成。<br>
     * 新しいスレッドを作成します。<br>
     * !スレ名・横幅・高さ
     * @throws SQLRuntimeException スレッドの作成が失敗した場合。
     */
    public void CreateThread() {

        String sqlCreateThread = "insert into thread(thread_name, width, height, last_update, created_date, addres)"
                + "values(?,?,?,now(),now(), ?)";
        try (Connection con = dataSource_.getConnection();
                PreparedStatement stmCreateThread = con.prepareStatement(sqlCreateThread)) {
            stmCreateThread.setString(1, threadName_);
            stmCreateThread.setInt(2, width_);
            stmCreateThread.setInt(3, height_);
            stmCreateThread.setString(4, addres_);
            stmCreateThread.execute();
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        }
    }

    /**
     * スレッドIDの設定。
     * @param threadId スレッドID
     */
    public void setThreadId(int threadId) {
        threadId_ = threadId;
    }

    /**
     * スレッド名の設定。
     * 空文字、NULL、スペースのみの設定は許可されていません。
     * @param threadName スレッド名
     * @throws GeneralErrorException スレッド名が不正な値
     */
    public void setThreadName(String threadName) {
        if (StringUtils.isBlank(threadName))
            throw new GeneralErrorException("スレッド名に不正な値を設定しようとしています。");
        threadName_ = threadName.trim();
    }

    /**
     * 横幅の設定。
     * @param width 描画枠横幅
     */
    public void setWidth(int width) {
        width_ = width;
    }

    /**
     * 高さの設定。
     * @param height 描画枠高さ
     */
    public void setHeight(int height) {
        height_ = height;
    }

    /**
     * 最終更新日の設定。
     * @param lastUpdate 最終更新日
     */
    public void setLastUpdate(String lastUpdate) {
        lastUpdate_ = Objects.requireNonNull(lastUpdate);
    }

    /**
     * 作成日の設定。
     * @param createdDate 作成日
     */
    public void setCreatedDate(String createdDate) {
        createdDate_ = Objects.requireNonNull(createdDate);
    }

    /**
     * 状態の設定。
     * @param isShow 状態
     */
    public void setIsShow(boolean isShow) {
        isShow_ = isShow;
    }

    /**
     * IPアドレスの設定。
     * @param addres IPアドレス
     */
    public void setAddres(String addres) {
        addres_ = addres;
    }

    /**
     * レスカウンタの設定。
     * @param resCount レスカウント
     */
    public void setResCount(int resCount) {
        resCount_ = resCount;
    }

    /**
     * スレッドIDの取得。
     * @return スレッドID
     */
    public int getThreadId() {
        return threadId_;
    }

    /**
     * スレッド名の取得。
     * @return スレッド名
     */
    public String getThreadName() {
        return threadName_;
    }

    /**
     * 横幅の取得。
     * @return 横幅
     */
    public int getWidth() {
        return width_;
    }

    /**
     * 高さの取得。
     * @return 高さ
     */
    public int getHeight() {
        return height_;
    }

    /**
     * 最終更新日の取得。
     * @return 最終更新日
     */
    public String getLastUpdate() {
        return lastUpdate_;
    }

    /**
     * 作成日の取得。
     * @return 作成日
     */
    public String getCreatedDate() {
        return createdDate_;
    }

    /**
     * 状態の取得。
     * @return 状態
     */
    public boolean isShow() {
        return isShow_;
    }

    /**
     * レスカウンタの取得。
     * @return レスカウンタ
     */
    public int getResCount() {
        return resCount_;
    }

}
