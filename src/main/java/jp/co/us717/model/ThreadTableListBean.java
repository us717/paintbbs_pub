package jp.co.us717.model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import jp.co.us717.exception.SQLRuntimeException;
import jp.co.us717.lib.BeanCommonFunction;

/**
 * スレッド一覧<br>
 */
public class ThreadTableListBean extends BeanCommonFunction {

    /** スレッド一覧リスト */
    private ArrayList<ThreadBean> threadTableList_;

    /**
     * コンストラクタ。
     */
    public ThreadTableListBean() {
        threadTableList_ = new ArrayList<>();
    }

    /**
     * スレッド一覧リストの作成。<br>
     * スレッドの更新が最新の順から、指定した件数までをデータベースから読み込み、<br>
     * スレッド一覧のリストを作成します。
     * @param numberOfThreadTable 表示する
     * @throws SQLRuntimeException データベース接続エラー
     */
    public void SelectThreadList(int numberOfThreadTable) {

        //!初期化
        threadTableList_.clear();

        //!読み込み
        String sqlSelectThreadTable = "select thread_id, thread_name, last_update, created_date, res_count "
                + "from thread where state=true order by last_update desc limit ?";
        try (Connection con = dataSource_.getConnection();
                PreparedStatement stmGetDeleteFileName = con.prepareStatement(sqlSelectThreadTable)) {

            //データ取得
            stmGetDeleteFileName.setInt(1, numberOfThreadTable);
            ResultSet rs = stmGetDeleteFileName.executeQuery();

            //データ格納
            while (rs.next()) {
                ThreadBean thread = new ThreadBean();
                thread.setThreadId(rs.getInt(1));
                thread.setThreadName(rs.getString(2));
                thread.setLastUpdate(rs.getString(3));
                thread.setCreatedDate(rs.getString(4));
                thread.setResCount(rs.getInt(5));
                threadTableList_.add(thread);
            }
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        }
    }

    /**
     * スレッド一覧リストの取得。
     * @return スレッド一覧リスト
     */
    public ArrayList<ThreadBean> getThreadTableList() {
        return threadTableList_;
    }
}
