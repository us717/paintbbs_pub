package jp.co.us717.model;

import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import jp.co.us717.exception.IORuntimeException;
import jp.co.us717.exception.SQLRuntimeException;
import jp.co.us717.lib.BeanCommonFunction;

/**
 * 古いスレッド・レスの削除。
 */
public class DeleteOldDataBean extends BeanCommonFunction {

    /**
     * コンストラクタ。
     */
    public DeleteOldDataBean() {
    }

    /**
     * スレッドの書き込み禁止指定。<br>
     * 各スレッドの最終書き込み日時が最新の順から、指定した順位を超えるスレッドを書き込み禁止にします。
     * @param orderRank 指定順位（指定した順位は含まない)
     * @throws SQLRuntimeException 書き込み禁止の処理に失敗した場合
     */
    public void ChangeThreadNoPosting(int orderRank) {
        String sqlWriteInhibit = "update thread set state = false where last_update in"
                + "(select last_update from thread order by last_update desc offset ?) and state = true";
        try (Connection con = dataSource_.getConnection();
                PreparedStatement stmWriteInhibit = con.prepareStatement(sqlWriteInhibit)) {

            //該当スレッドを書き込み禁止にする
            stmWriteInhibit.setInt(1, orderRank);
            stmWriteInhibit.execute();
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        }
    }

    /**
     * 古いスレッド・レスの削除。<br>
     * 各スレッドの最終書き込み日時が最新の順から、指定した順位を超えるスレッド・レス・画像ファイルを削除します。
     * @param orderRank 指定順位(指定した順位は含まない)
     * @throws SQLRuntimeException   データベースの削除に失敗した場合
     * @throws IORuntimeException    画像ファイルの削除に失敗した場合
     */
    public void DeleteOldData(int orderRank) {
        String sqlGetDeleteFileName = "select filename from res where thread_id in" //削除対象のファイル名取得
                + "(select thread_id from thread order by last_update desc offset ?)";

        String sqlDeleteRes = "delete from Res where thread_id in" //対象レス・スレッド削除
                + "(select thread_id from thread order by last_update desc offset ?)";

        String sqlDeleteThread = "delete from Thread where thread_id in" //対象レス・スレッド削除
                + "(select thread_id from thread order by last_update desc offset ?)";
        try (Connection con = dataSource_.getConnection();
                PreparedStatement stmGetDeleteFileName = con.prepareStatement(sqlGetDeleteFileName);
                PreparedStatement stmDeleteRes = con.prepareStatement(sqlDeleteRes);
                PreparedStatement stmDeleteThread = con.prepareStatement(sqlDeleteThread)) {

            //!該当スレッドとレス及び画像ファイルの削除
            try {
                con.setAutoCommit(false);

                //削除対象のファイル名取得
                stmGetDeleteFileName.setInt(1, orderRank);
                ResultSet deleteFileName = stmGetDeleteFileName.executeQuery();

                //該当レスの削除
                stmDeleteRes.setInt(1, orderRank);
                stmDeleteRes.execute();

                //該当スレッドの削除
                stmDeleteThread.setInt(1, orderRank);
                stmDeleteThread.execute();

                con.commit();

                //ファイル削除
                while (deleteFileName.next()) {
                    Path path = FileSystems.getDefault().getPath(realPath_, deleteFileName.getString(1));
                    Files.deleteIfExists(path);
                }
            } catch (SQLException ex) {
                con.rollback();
                throw ex;
            } finally {
                con.setAutoCommit(true);
            }
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } catch (IOException e) {
            throw new IORuntimeException(e);
        }
    }
}
