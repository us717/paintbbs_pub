package jp.co.us717.model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Objects;

import jp.co.us717.exception.SQLRuntimeException;
import jp.co.us717.lib.BeanCommonFunction;

import org.apache.commons.lang3.StringUtils;

/**
 * 見出し。<br>
 * 指定したページに表示するスレッドリストと、スレッド内容として、<br>
 * 最新のレス情報のみが格納されたレステーブルを作成されます<br>
 */
public class HeadlineBean extends BeanCommonFunction {

    /** 最新のレス扱いとなる件数 */
    private int numberOfNewRes_;

    /** 1ページに表示する件数 */
    private int numberOfArticle_;

    /** 現在のページ番号 */
    private int pageNumber_;

    /** スレッド一覧リスト */
    private ArrayList<ThreadBean> threadTableList_;

    /** 見出しスレッドリスト */
    private ArrayList<ThreadBean> headlineThreadList_;

    /** 見出しレス テーブル*/
    private HashMap<Integer, ArrayList<ResBean>> headlineResTable_;//スレッドid:データ

    /**
     * コンストラクタ。
     */
    public HeadlineBean() {
        headlineThreadList_ = new ArrayList<>();
        headlineResTable_ = new HashMap<>();
    }

    /**
    * 見出し作成。<br>
    * 指定されたスレッド一覧の情報を元に、指定ページで表示するスレッドリスト及び、スレ内容として最新のレスのみのレスリストを作成します。<br>
    * また、最新の情報を反映するために、表示対象のスレッドのみを対象に、スレッド一覧リストのスレッド情報が最新のものに変更されます。<br>
    * -スレッドは最終更新日が最新の順で取得されます。<br>
    * -スレッド一覧の作成には、ThreadTableListBeanが有効です。<br>
    *
    * !最新のレス、表示件数、ページ番号、スレッド一覧リスト
    * @throws SQLRuntimeException   データベースでのエラーが発生した場合
    * @see ThreadTableListBean
    */
    public void SelectHeadline() {

        //!初期化・変数定義
        headlineThreadList_.clear();
        headlineResTable_.clear();
        int articleBegin = numberOfArticle_ * pageNumber_; //記事通し番号(ページはじめ)
        int articleEnd = numberOfArticle_ * (pageNumber_ + 1);//記事通し番号(ページ終わり)

        //!見出しスレッドリスト作成
        for (int i = articleBegin; i < articleEnd && i < threadTableList_.size(); i++) {

            ThreadBean tmpRecord = new ThreadBean();
            tmpRecord.setThreadId(threadTableList_.get(i).getThreadId());
            tmpRecord.setThreadName(threadTableList_.get(i).getThreadName());
            tmpRecord.setCreatedDate(threadTableList_.get(i).getCreatedDate());
            headlineThreadList_.add(tmpRecord);

            int tmpThreadId = threadTableList_.get(i).getThreadId();
            headlineResTable_.put(tmpThreadId, new ArrayList<ResBean>());//空のレスリストも作成しておく
        }

        //!処理を継続するかチェック
        if (headlineThreadList_.size() == 0) {
            return; //表示対象となるスレッドが存在しなければこれ以上処理しない
        }

        //!見出しレステーブル作成
        StringBuilder sqlSelectResList = new StringBuilder();
        sqlSelectResList
                .append("select thread_id, poster, postdate, is_password, filename, state, res_num from"
                        + "(select thread_id, poster, postdate, is_password, filename, state,"
                        + "row_number() over(partition by thread_id order by postdate desc)index_num,"
                        + "row_number() over(partition by thread_id order by postdate asc)res_num "
                        + "from res)result where index_num <= ? and thread_id in(");
        sqlSelectResList.append(StringUtils.join(Collections.nCopies(headlineThreadList_.size(), "?").toArray(), ","));
        sqlSelectResList.append(")");

        try (Connection con = dataSource_.getConnection();
                PreparedStatement stmSelectNewRes = con.prepareStatement(sqlSelectResList.toString())) {

            //!データ取得
            stmSelectNewRes.setInt(1, numberOfNewRes_);
            for (int i = 0; i < headlineThreadList_.size(); i++) {
                int tmpThreadId = headlineThreadList_.get(i).getThreadId();
                stmSelectNewRes.setInt(i + 2, tmpThreadId);
            }
            ResultSet rs = stmSelectNewRes.executeQuery();

            //!対象のスレッドごとにレスリストを作成
            while (rs.next()) {
                ResBean tmpRes = new ResBean();
                tmpRes.setThreadId(rs.getInt(1));
                tmpRes.setPoster(rs.getString(2));
                tmpRes.setPostDate(rs.getString(3));
                tmpRes.setIsPassword(rs.getBoolean(4));
                tmpRes.setImagePath(rs.getString(5));
                tmpRes.setIsShow(rs.getBoolean(6));
                tmpRes.setResNum(rs.getInt(7));
                headlineResTable_.get(rs.getInt(1)).add(tmpRes);
            }

            //!レス数を最新のものに更新
            for (int i = 0; i < headlineThreadList_.size(); i++) {
                int tmpThreadId = headlineThreadList_.get(i).getThreadId(); //対象のスレッドID
                int tmpResSize = headlineResTable_.get(tmpThreadId).size(); //対象スレッドのレス数

                //見出しスレッドリストにレス数を登録
                headlineThreadList_.get(i).setResCount(tmpResSize);

                //スレッド一覧リスト修正、最新のレス数を登録
                if (tmpResSize != 0) {
                    int lastResNum = headlineResTable_.get(tmpThreadId).get(tmpResSize - 1).getResNum();
                    threadTableList_.get(articleBegin + i).setResCount(lastResNum);
                } else {
                    threadTableList_.get(articleBegin + i).setResCount(0);
                }
            }
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        }
    }

    /**
     * 最新のレス扱いとなる件数の設定。
     * @param numberOfNewRes 最新のレス扱いとなる件数
     */
    public void setNumberOfNewRes(int numberOfNewRes) {
        numberOfNewRes_ = numberOfNewRes;
    }

    /**
     * 1ページ表示する件数の設定。
     * @param numberOfArticle 1ページに表示する件数
     */
    public void setNumberOfArticle(int numberOfArticle) {
        numberOfArticle_ = numberOfArticle;
    }

    /**
     * 現在のページ番号の設定。
     * @param pageNumber 現在のページ番号
     */
    public void setPageNumber(int pageNumber) {
        pageNumber_ = pageNumber;
    }

    /**
     * スレッド一覧リストの設定。
     * @param threadTableList スレッド一覧リスト
     */
    public void setThreadTableList(ArrayList<ThreadBean> threadTableList) {
        threadTableList_ = Objects.requireNonNull(threadTableList);
    }

    /**
     * 再生のレス扱いとなる件数の取得。
     * @return 最新のレス扱いとなる件数
     */
    public int getNumberOfNewRes() {
        return numberOfNewRes_;
    }

    /**
     * 1ページに表示する件数の取得。
     * @return 1ページに表示する件数
     */
    public int getNumberOfArticle() {
        return numberOfArticle_;
    }

    /**
     * 現在のページ番号の取得。
     * @return 現在のページ番号
     */
    public int getPageNumber() {
        return pageNumber_;
    }

    /**
     * 最新のスレッドリスト取得。
     * @return 最新のスレッドリスト
     */
    public ArrayList<ThreadBean> getHeadlineThreadList() {
        return headlineThreadList_;
    }

    /**
     * 最新のレスリスト取得。
     * @return 最新のレステーブル
     */
    public HashMap<Integer, ArrayList<ResBean>> getHeadlineResTable() {
        return headlineResTable_;
    }
}