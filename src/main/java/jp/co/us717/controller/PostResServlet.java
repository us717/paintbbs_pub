package jp.co.us717.controller;

import java.io.IOException;
import java.util.Objects;

import javax.annotation.Resource;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import jp.co.us717.exception.IORuntimeException;
import jp.co.us717.exception.SQLRuntimeException;
import jp.co.us717.lib.CookieTable;
import jp.co.us717.model.ResBean;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * レスの投稿。
 */
@WebServlet("/PostResServlet")
public class PostResServlet extends HttpServlet {

    private static final long serialVersionUID = 6089920261669310837L;
    final static Logger log__ = LogManager.getLogger();

    /** データベースコンテキスト格納 */
    @Resource(name = "jdbc/db")
    private DataSource dataSource_ = null;

    /**
     * 初期化。
     */
    @Override
    public void init() throws ServletException {
        //データベースの設定を読み込む
        try {
            Context context = new InitialContext();
            dataSource_ = (DataSource) context.lookup("java:comp/env/jdbc/db");
        } catch (NamingException e) {
            log__.error(e);
            throw new ServletException(e);
        }
    }

    /**
     * Postメソッド。
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException,
            IOException {
        request.setCharacterEncoding("UTF-8");

        //クッキーチェック(連続投稿禁止)
        CookieTable cookieTable = new CookieTable();
        cookieTable.CreateCookieTable(request.getCookies());
        if (cookieTable.getTableSize() != 0 && cookieTable.isCookie("post")) {
            Cookie tmpCookie = cookieTable.getCookie("post");
            tmpCookie.setMaxAge(30);
            response.addCookie(tmpCookie);
            String path = getServletContext().getContextPath() + "/jsp/warning.jsp?id="
                    + request.getParameter("id");
            response.sendRedirect(path);
            return;
        }

        //変数設定
        ResBean postRes = new ResBean();
        postRes.setDBConnect(dataSource_);
        postRes.setSaveLocation(getServletContext().getRealPath("/Draw"));

        //パラメータ設定
        String num = Objects.requireNonNull(request.getParameter("id"));
        postRes.setThreadId(Integer.parseInt(num));
        postRes.setImageData(Objects.requireNonNull(request.getParameter("paintData")));
        postRes.setPassword(Objects.requireNonNull(request.getParameter("password")));
        postRes.setPoster(Objects.requireNonNull(request.getParameter("poster")));
        postRes.setAddres(request.getRemoteAddr());

        //レス投稿
        try {
        postRes.InsertRes();
        } catch(SQLRuntimeException | IORuntimeException e) {
            log__.warn(e);
            throw e;
        }

        //クッキー発行
        Cookie cookie = new Cookie("post", "1");
        cookie.setMaxAge(30);
        response.addCookie(cookie);

        //リダイレクト
        String path = getServletContext().getContextPath() + "/jsp/success_res.jsp?id=" + request.getParameter("id");
        response.sendRedirect(path);
    }
}
