package jp.co.us717.controller;

import java.io.IOException;

import javax.annotation.Resource;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import jp.co.us717.exception.GeneralErrorException;
import jp.co.us717.exception.SQLRuntimeException;
import jp.co.us717.model.ThreadDataBean;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * スレッドと内容(レス)の表示。
 */
@WebServlet("/ShowThreadServlet")
public class ShowThreadServlet extends HttpServlet {

    private static final long serialVersionUID = 6089920261669310837L;
    final static Logger log__ = LogManager.getLogger();

    /** データベースコンテキスト格納 */
    @Resource(name = "jdbc/db")
    private DataSource dataSource_ = null;

    /**
     * 初期化。
     */
    @Override
    public void init() throws ServletException {
        //データベースの設定を読み込む
        try {
            Context context = new InitialContext();
            dataSource_ = (DataSource) context.lookup("java:comp/env/jdbc/db");
        } catch (NamingException e) {
            log__.error(e);
            throw new ServletException(e);
        }
    }

    /**
     * Getメソッド。
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        //変数設定
        ThreadDataBean threadData = new ThreadDataBean();
        threadData.setDBConnect(dataSource_);

        //パラメータ設定
        String tmpThreadId = request.getParameter("thread");
        if (tmpThreadId == null) {
            throw new GeneralErrorException();
        }
        int threadId = Integer.parseInt(tmpThreadId);

        //DBから情報取得
        threadData.setThreadId(threadId);
        try {
        threadData.SelectThreadData();
            if (threadData.getThreadInfo().getThreadId() == 0 || !threadData.getThreadInfo().isShow()) {
                throw new GeneralErrorException();
            }
        } catch (SQLRuntimeException e ) {
            log__.warn(e);
            throw e;
        }

        //フォワード
        request.setAttribute("threadInfo", threadData.getThreadInfo());
        request.setAttribute("resList", threadData.getResList());
        RequestDispatcher dis = getServletContext().getRequestDispatcher("/WEB-INF/jsp/threadF.jsp");
        dis.forward(request, response);
    }
}
