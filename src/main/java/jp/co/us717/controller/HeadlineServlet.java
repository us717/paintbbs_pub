package jp.co.us717.controller;

import java.io.IOException;
import java.util.Objects;

import javax.annotation.Resource;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import jp.co.us717.exception.SQLRuntimeException;
import jp.co.us717.model.HeadlineBean;
import jp.co.us717.model.ThreadTableListBean;

/**
 * スレッド一覧、見出し。
 */
@WebServlet("/HeadlineServlet")
public class HeadlineServlet extends HttpServlet {

    private static final long serialVersionUID = -7998609881713508412L;
    final static Logger log__ = LogManager.getLogger();

    /** データベースコンテキスト格納 */
    @Resource(name = "jdbc/db")
    private DataSource dataSource_ = null;

    /**
     * 初期化
     */
    @Override
    public void init() throws ServletException {
        //データベースの設定を読み込む
        try {
            Context context = new InitialContext();
            dataSource_ = (DataSource) context.lookup("java:comp/env/jdbc/db");
        } catch (NamingException e) {
            log__.error(e);
            throw new ServletException(e);
        }
    }

    /**
     * Getメソッド
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        //変数設定
        ThreadTableListBean threadTable = new ThreadTableListBean();
        HeadlineBean headline = new HeadlineBean();
        threadTable.setDBConnect(dataSource_);
        headline.setDBConnect(dataSource_);

        //パラメータ設定
        String tmpPage = request.getParameter("page");
        int page = Objects.nonNull(tmpPage) ? Integer.parseInt(tmpPage) : 0; //表示するページ番号
        int threadTableNum = 30;//読み込むスレッド数
        int newResNum = 5;//最新のレス扱いになる件数
        int articleNum = 5; //1ページに表示できる件数

        //DBから情報取得
        try {
            threadTable.SelectThreadList(threadTableNum);
            headline.setNumberOfNewRes(newResNum);
            headline.setNumberOfArticle(articleNum);
            headline.setPageNumber(page);
            headline.setThreadTableList(threadTable.getThreadTableList());
            headline.SelectHeadline();
        } catch(SQLRuntimeException e) {
            log__.warn(e);
            throw e;
        }
        //フォワード
        request.setAttribute("page", page);
        request.setAttribute("articleNum", articleNum);
        request.setAttribute("headline", headline);
        request.setAttribute("threadTable", threadTable.getThreadTableList());
        request.setAttribute("headlineThread", headline.getHeadlineThreadList());
        request.setAttribute("headlineRes", headline.getHeadlineResTable());
        RequestDispatcher dis = getServletContext().getRequestDispatcher("/WEB-INF/jsp/headlineF.jsp");
        dis.forward(request, response);
    }
}
