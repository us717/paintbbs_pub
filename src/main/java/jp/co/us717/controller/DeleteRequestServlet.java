package jp.co.us717.controller;

import java.io.IOException;

import javax.annotation.Resource;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import jp.co.us717.exception.SQLRuntimeException;
import jp.co.us717.lib.ContentsTypeEnum;
import jp.co.us717.model.DeletionRequestBean;
import jp.co.us717.model.ResBean;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * 削除依頼。
 */
@WebServlet("/DeleteRequestServlet")
public class DeleteRequestServlet extends HttpServlet {

    private static final long serialVersionUID = 6089920261669310837L;
    final static Logger log__ = LogManager.getLogger();

    /** データベースコンテキスト格納 */
    @Resource(name = "jdbc/db")
    private DataSource dataSource_ = null;

    /**
     * 初期化。
     */
    @Override
    public void init() throws ServletException {
        //データベースの設定を読み込む
        try {
            Context context = new InitialContext();
            dataSource_ = (DataSource) context.lookup("java:comp/env/jdbc/db");
        } catch (NamingException e) {
            log__.error(e);
            throw new ServletException(e);
        }
    }

    /**
     * Postメソッド。
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException,
            IOException {

        //!変数設定
        DeletionRequestBean deleteRequest = new DeletionRequestBean();
        ResBean res = new ResBean();
        deleteRequest.setDBConnect(dataSource_);
        res.setDBConnect(dataSource_);

        //!レス削除・削除依頼
        String type = request.getParameter("type");
        if (type.equals("deleteRes")) {
            try {
                //レス削除(表示禁止へ)
                res.setThreadId(Integer.parseInt(request.getParameter("threadId")));
                res.setResNum(Integer.parseInt(request.getParameter("resNum")));
                res.setPassword(request.getParameter("delPassword"));
                res.ProhibitionOfResShowing();

                //成功ページへ
                String path = getServletContext().getContextPath() + "/jsp/success_delete.jsp?id="
                        + request.getParameter("threadId");
                response.sendRedirect(path);

            } catch (SQLRuntimeException e) {
                //ユーザーの入力ミスの可能性があるので特殊なエラーページへ
                String path = getServletContext().getContextPath() + "/jsp/error_delete.jsp";
                response.sendRedirect(path);
            }
        } else if (type.equals("deleteRequest")) {
            try {
                //削除依頼
                deleteRequest.setRequestType(ContentsTypeEnum.RES);
                deleteRequest.setThreadId(Integer.parseInt(request.getParameter("threadId")));
                deleteRequest.setThreadNum(Integer.parseInt(request.getParameter("resNum")));
                deleteRequest.setReason(Integer.parseInt(request.getParameter("reason")));
                deleteRequest.setIpAddres(request.getRemoteAddr());
                deleteRequest.RegisterRequestResDelete();
            } catch (SQLRuntimeException e) {
                log__.warn(e);
                throw e;
            }
            //成功ページへ
            String path = getServletContext().getContextPath() + "/jsp/acceptance_delete.jsp";
            response.sendRedirect(path);
        }
    }
}