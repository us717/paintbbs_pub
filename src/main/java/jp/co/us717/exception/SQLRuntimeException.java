package jp.co.us717.exception;

import java.sql.SQLException;

/**
 * SQLエラー。
 * SQLExceptionの実行時例外バージョン
 */
public class SQLRuntimeException extends RuntimeException {

    private static final long serialVersionUID = 2208741611595173267L;

    /**
     * コンストラクタ
     */
    public SQLRuntimeException() {
        super();
    }

    /**
     * コンストラクタ。
     * メッセージあり
     * @param message   警告メッセージ
     */
    public SQLRuntimeException(String message) {
        super(message);
    }

    /**
     * コンストラクタ
     * SQLException引き継ぎ
     * @param e SQLException
     */
    public SQLRuntimeException(SQLException e) {
        super(e);
    }
}

