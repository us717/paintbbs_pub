package jp.co.us717.exception;

import java.io.IOException;

/**
 * IOエラー。
 * IOExceptionの実行時例外バージョン
 */
public class IORuntimeException extends RuntimeException {

    private static final long serialVersionUID = -3101465366724074043L;

    /**
     * コンストラクタ
     */
    public IORuntimeException() {
        super();
    }

    /**
     * コンストラクタ。
     * メッセージあり
     * @param message   警告メッセージ
     */
    public IORuntimeException(String message) {
        super(message);
    }

    /**
     * コンストラクタ
     * IOException引き継ぎ
     * @param e SQLException
     */
    public IORuntimeException(IOException e) {
        super(e);
    }
}
