package jp.co.us717.lib;

import java.util.HashMap;
import java.util.Objects;

import javax.servlet.http.Cookie;

/**
 * クッキーテーブル
 * requestから取得したクッキーデータの配列を、使いやすいようHashMap形式にします。
 */
public class CookieTable {

    HashMap<String, Cookie> cookieTable_;

    /**
     * コンストラクタ。
     */
    public CookieTable() {
        cookieTable_ = new HashMap<>();
    }

    /**
     * クッキーテーブルの作成。
     * @param cookies 取得したクッキー
     * @return 作成に成功したか
     */
    public boolean CreateCookieTable(Cookie[] cookies) {
        if (cookies == null) {
            return false;
        }

        for (int i = 0; i < cookies.length; i++) {
            cookieTable_.put(cookies[i].getName(), cookies[i]);
        }
        return true;
    }

    /**
     * クッキーの取得。
     * @param name 取得したいクッキー名
     * @return 取得したクッキー
     */
    public Cookie getCookie(String name) {
        return cookieTable_.get(Objects.requireNonNull(name));
    }

    /**
     * クッキーが存在するかチェック。
     * @param name 確認したいクッキー名
     * @return クッキーが存在しているか
     */
    public boolean isCookie(String name) {
        if (cookieTable_.get(Objects.requireNonNull(name)) == null) {
            return false;
        }
        return true;
    }

    /**
     * クッキーテーブルの大きさ取得。
     * @return テーブルのサイズ
     */
    public int getTableSize() {
        return cookieTable_.size();
    }
}
