package jp.co.us717.lib;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Objects;

import jp.co.us717.exception.SQLRuntimeException;

/**
 * データベース操作の汎用機能まとめ。
 *
 */
public class DatabaseFunction {

    /**
     * レコード作成。
     * 各カラムをマッピングして、ひとつのレコードデータを作成する。
     * resultSetの移動は行っていないので外部で実行すること。
     * @param rs      取得済みResultSet
     * @param rsmd    取得済みResultSetMetaData
     * @return 1レコード分のデータ
     * @throws SQLRuntimeException データベースエラー
     */
    public HashMap<String, String> CreateRecord(ResultSet rs, ResultSetMetaData rsmd) {

        HashMap<String, String> record = new HashMap<>();

        //引数チェック
        if (Objects.isNull(rs) || Objects.isNull(rsmd)) {
            return record;
        }

        //レコード作成
        try {
            for (int i = 1; i <= rsmd.getColumnCount(); i++) {
                String columnName = rsmd.getColumnName(i);
                String date = rs.getString(i);
                record.put(columnName, date);
            }
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        }
        return record;
    }

    /**
     * レコードリスト作成。
     * ResultSetに入ったデータを読み込み、レコードリストを作成します。
     * @param rs 取得済みResultSet
     * @return 各レコードの入ったリスト
     * @throws SQLRuntimeException データベースエラー
     */
    public ArrayList<HashMap<String, String>> CreateRecordList(ResultSet rs) {

        ArrayList<HashMap<String, String>> recordList = new ArrayList<>();

        //引数チェック
        if (Objects.isNull(rs)) {
            return recordList;
        }

        //レコード作成
        try {
            ResultSetMetaData resultSetMetaData = rs.getMetaData();
            while (rs.next()) {
                HashMap<String, String> record = new HashMap<>();
                for (int i = 1; i <= resultSetMetaData.getColumnCount(); i++) {
                    String columnName = resultSetMetaData.getColumnName(i);
                    String date = rs.getString(i);
                    record.put(columnName, date);
                }
                recordList.add(CreateRecord(rs, resultSetMetaData));
            }
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        }

        return recordList;
    }
}
