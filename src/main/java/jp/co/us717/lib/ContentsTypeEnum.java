package jp.co.us717.lib;

/**
 * コンテンツタイプ。
 */
public enum ContentsTypeEnum {
    THREAD,
    RES;
}
