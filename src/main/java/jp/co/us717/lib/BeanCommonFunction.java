package jp.co.us717.lib;

import java.util.Objects;

import javax.sql.DataSource;

/**
 * Beanクラス用の共通機能。<br>
 * Beanクラス作成時によく定義する機能をまとめています。
 */
public abstract class BeanCommonFunction {

    /** 取得済みデータソース **/
    protected DataSource dataSource_;

    /** ファイルシステム上の絶対パス */
    protected String realPath_;

    /**
    * データベース接続用情報の設定。
    * @param tmpDataSource JNDIから取得済みのDataSource
    */
    public void setDBConnect(DataSource tmpDataSource) {
        dataSource_ = Objects.requireNonNull(tmpDataSource);
    }

    /**
     * ファイルシステム上の絶対パス設定。<br>
     * ServletContextなどから取得できるファイルシステム上の絶対パスを設定し、<br>
     * ディレクトリ名などと組み合わせてBean内で利用することにより、アプリケーションの配置場所に依存しない<br>
     * ファイル操作が可能になります。
     * @param realPath ファイルシステム上の絶対パス
     */
    public void setSaveLocation(String realPath) {
        realPath_ = Objects.requireNonNull(realPath);
    }
}
